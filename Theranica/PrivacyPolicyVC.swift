//
//  PrivacyPolicyVC.swift
//  Theranica
//
//  Created by MSApps on 13/06/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit

class PrivacyPolicyVC: UIViewController{
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var aproveBtn: UIButton!
    private let urlStr = "http://theranica.com/privacy-policy/"
    private var buttonColor : UIColor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let blueColor = aproveBtn.backgroundColor {
            buttonColor = blueColor
            aproveBtn.backgroundColor = UIColor.lightGray
            aproveBtn.isEnabled = false
        }

        webView.scrollView.delegate = self;
        
        // Do any additional setup after loading the view.
        if let url = URL(string: urlStr){
            webView.loadRequest(URLRequest(url: url))
        }
        
    }
    
    @IBAction func aproveBtnClicked(_ sender: UIButton) {
        DBManager.manager.saveIsEulaAproved(isAproved: true)
        RoutingController.shared.determineRoot()
    }
    

}

extension PrivacyPolicyVC : UIScrollViewDelegate{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= (scrollView.contentSize.height - (scrollView.frame.size.height * 2)){
            aproveBtn.isEnabled = true
            aproveBtn.backgroundColor = buttonColor
        }
    }
}
