//
//  DetailsMigraineDiaryCell.swift
//  Theranika
//
//  Created by msapps on 21/06/2017.
//  Copyright © 2017 msapps. All rights reserved.
//

import UIKit

class DetailsMigraineDiaryCell: UITableViewCell {
    
    @IBOutlet weak var subject: UILabel!
    @IBOutlet weak var subjectValue: DetailsMigraineDiaryTextField!
    
    var pickOption = [String]()
    var symptom : Symptom?
    var index : Int?
    var delegate : DetailsMigraineDiaryCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupPicker()
    }
    
    var toolBar : UIToolbar {
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: 44))
        toolBar.barStyle = UIBarStyle.default
        toolBar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(textFieldDoneEditing(_:))),
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        ]
        toolBar.sizeToFit()
        return toolBar
    }
    
    func configCell(with symptom : Symptom, at index : Int){
        self.symptom = symptom
        self.index = index
        self.subject.text = symptom.name?.propercased
        self.subjectValue.text = symptom.painLevel ?? symptom.headacheSide
        self.fetchPickerOptions()
    }
    
    func setupPicker(){
        let picker = UIPickerView()
        picker.delegate = self
        subjectValue.inputView = picker
        subjectValue.inputAccessoryView = toolBar
        //subjectValue.delegate = self
        
    }
    
    func fetchPickerOptions(){
        
        guard let symptom = symptom else{
            return
        }
        
        // add Picker options
        if symptom.headacheSide != nil {
            pickOption = Symptom.HeadacheSide.values
        }else{
            pickOption = Symptom.PainLevel.values
            if symptom.name == SymptomQuestion.headache_level.title{
                for i in 0 ..< self.pickOption.count{
                    if pickOption[i] == Symptom.PainLevel.none.rawValue{
                        pickOption.remove(at: i)
                        return;
                    }
                }
            }
        }
        
    }
    
    @IBAction func textFieldDoneEditing(_ sender: UITextField) {
        if let symptom = symptom, let index = index{
            self.delegate?.symptomDidUpdate(at: index, symptom: symptom)
        }
        subjectValue.resignFirstResponder()
    }
    
}
extension DetailsMigraineDiaryCell :  UIPickerViewDataSource, UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickOption[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let selectedOption = pickOption[row]
        subjectValue.text = selectedOption
        
        // check the value from picker, if not found return nil
        let painLevel    = Symptom.PainLevel.enumCase(from: selectedOption)
        let headacheSide = Symptom.HeadacheSide.enumCase(from: selectedOption)
        
        self.symptom?.painLevel = painLevel?.rawValue
        self.symptom?.headacheSide = headacheSide?.rawValue
    }
}

protocol DetailsMigraineDiaryCellDelegate {
    func symptomDidUpdate(at index : Int, symptom : Symptom)
}
