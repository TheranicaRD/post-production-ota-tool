//
//  UIView.swift
//  Theranica
//
//  Created by Maor Shams on 21/02/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit
extension UIView{
    
    func circled(with radius : CGFloat? = nil){
        self.layer.cornerRadius = radius == nil ? (self.frame.width / 2) : radius!
        self.clipsToBounds = true
    }
    
    func bordered(with width : CGFloat){
        self.layer.borderWidth = width
        self.clipsToBounds = true
    }
    
    func topBordered( height: CGFloat = 1, color : UIColor = .gray){
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x: 0, y: 0, width: bounds.size.width, height: height)
        bottomBorder.backgroundColor = color.cgColor
        self.layer.addSublayer(bottomBorder)
    }
    
    func bottomBordered( height: CGFloat = 1, color : UIColor = .gray){
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x: 0, y:  bounds.size.height - 1, width: bounds.size.width, height: height)
        bottomBorder.backgroundColor = color.cgColor
        self.layer.addSublayer(bottomBorder)
    }
    
    func leftBordered(width: CGFloat = 1) -> UIView{
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x: 0, y: 0, width: 1, height: bounds.size.height)
        bottomBorder.backgroundColor = UIColor.gray.cgColor
        self.layer.addSublayer(bottomBorder)
        return self
    }
    
}
