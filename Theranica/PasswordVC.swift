//
//  PasswordVC.swift
//  Theranica
//
//  Created by MSApps on 13/02/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit

class PasswordVC: UIViewController {

    @IBOutlet weak var openButton: UIButton!
    @IBOutlet weak var passwordTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.passwordTextfield.addToolBar(with: [doneButton])
        self.passwordTextfield.delegate = self
        self.openButton.isEnabled = false;
    }
    

    @IBAction func openBtnClicked(_ sender: UIButton) {
        
        passwordTextfield.resignFirstResponder()
        
        if passwordTextfield.text == Constants.APP_PASSWORD{
            DBManager.manager.saveIsEnteredAppPassword()
            RoutingController.shared.determineRoot()
        }else{
            //display password is incorrect
            _ = Utils.alert(title: "error".localized, message: "invalid_app_password_error".localized, type: .error)
            self.passwordTextfield.text?.removeAll()
            self.textChanged(passwordTextfield)
        }
    }

    @IBAction func textChanged(_ sender: UITextField) {
        let enabledColor = UIColor(red:0.03, green:0.41, blue:0.6, alpha:1.0)
        let disabledColor = UIColor(white:0.7, alpha:1.0)
        
        if (sender.text?.count ?? 0) >= 6{
            self.openButton.backgroundColor = enabledColor
            self.openButton.isEnabled = true
        }else{
            self.openButton.backgroundColor = disabledColor
            self.openButton.isEnabled = false
        }
    }
    
    private var doneButton : UIBarButtonItem{
        return UIBarButtonItem(barButtonSystemItem: .done,
                               target: self,
                               action: #selector(doneButtonAction))
    }

    @objc func doneButtonAction(){
        passwordTextfield.resignFirstResponder()
    }
    
}
extension PasswordVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= 6
    }
}
