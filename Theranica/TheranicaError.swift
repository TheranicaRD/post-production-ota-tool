//
//  TError.swift
//  Theranica
//
//  Created by Maor Shams on 14/02/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import Foundation
public enum TheranicaError : Error{
    
    case noResponseData
    case noValueModified
    case authenticationError
    case noTokenError
    case noUserConnected
    case noInternetConnection
    case deviceNotFound
    case bluetoothOff
    
    var localizedDescription : String{
        switch self {
        case .deviceNotFound : return "no_device_title"
        case .bluetoothOff   : return "Bluetooth is off"
        case .noResponseData: return "No Response Data"
        case .noValueModified: return "No Value Modified"
        case .authenticationError: return "Authentication Error"
        case .noTokenError : return "No token"
        case .noUserConnected : return "No User Connected"
        case .noInternetConnection : return "No Internet connection."
        }
    }
}
