//
//  AuthManager.swift
//  Theranica
//
//  Created by Maor Shams on 20/08/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit
import FacebookLogin
import FirebaseAuth
import Alamofire
import Google
import Firebase

protocol AuthManagerDelegate {
    func didFinishFacebookAuth(error : Error?)
    func didFinishGoogleAuth(error : Error?)
}

class AuthManager: NSObject {
    
    static let manager = AuthManager()
    var delegate  : AuthManagerDelegate?
    
    override private init() {
        super.init()
        GIDSignIn.sharedInstance().delegate = self
    }
    
    var currentUser : User?{
        return Auth.auth().currentUser
    }
    
    var currentUserToken : String? {
        return UserDefaults.standard.string(forKey: Constants.CURRENT_USER_TOKEN)
    }
    
    var currentUserProviderID : String?{
        return UserDefaults.standard.string(forKey: Constants.CURRENT_PROVIDER_ID)
    }
    
    var baseURL : URL{
        return URL(string: "https://secure-taiga-91414.herokuapp.com")!
    }
    
    var userRegURL : URL{
        return baseURL.appendingPathComponent("/userReg")
    }
    
    var testTokenURL : URL{
        return baseURL.appendingPathComponent("/testToken")
    }
    
    var logoutURL : URL{
        return baseURL.appendingPathComponent("/logout")
    }
    
    var logInWithMailURL : URL{
        return baseURL.appendingPathComponent("/login")
    }
    
    func isTokenValid(token : String ,completion:@escaping (Bool) -> ()) {
        let headers = ["x-auth": token]
        Alamofire.request(testTokenURL, method: .post, parameters: nil
            , encoding: JSONEncoding.default, headers: headers).validate().responseString { response in
                
                switch(response.result) {
                case .success(_):
                    if response.response?.statusCode == 200 {
                        print("isTokenValid successful")
                        completion(true)
                    }else{
                        completion(false)
                    }
                    
                    break
                    
                case .failure(_):
                    print(response.result.error as Any)
                    print("isTokenValid failed")
                    completion(false)
                    break
                    
                }
        }
    }
    
    
    
    /// Register via mail
    func requestMailAuth(password : String , firstName : String , lastName : String, email : String,completion : @escaping (Error?)->Void){
        
        var params = ["firstName":firstName,
                      "lastName":lastName]
        
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            
            guard let user = user else {
                completion(error)
                return
            }
            
            self.getFirebaseRefreshedToken(currentUser: user, completion: { (token, providerID) in
                
                guard let token = token, let providerID = providerID else{
                    completion(TheranicaError.noTokenError)
                    return
                }
                
                AuthManager.manager.saveCurrentUserDataLocally(token: token, providerID: providerID)
                
                params.updateValue(providerID, forKey: "providerID")
                
                self.sendAdditionalUserData(token: token , parameters: params, completion: { (error) in
                    completion(error)
                })
            })
        }
    }
    
    /// Sign in via Google
    func requestGmailAuth(){
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().signIn()
    }
    
    /// Sign in via Facebook
    func requestFacebookAuth(on vc:UIViewController){
        LoginManager().logOut()
        LoginManager().logIn([.publicProfile, .email, .userFriends], viewController: vc) { (result) in
            
            switch result{
            case .cancelled:
                self.delegate?.didFinishFacebookAuth(error: nil)
                break
                
            case .failed(let error):
                self.delegate?.didFinishFacebookAuth(error: error)
                break
                
            case .success( _,_, let token):
                
                let credential = FacebookAuthProvider.credential(withAccessToken: token.authenticationToken)
                
                self.fireBaseAuth(credential: credential){ error in
                    
                    self.delegate?.didFinishFacebookAuth(error: error)
                    RoutingController.shared.determineRoot()
                    
                }
                
            }
        }
    }
    
    /// Sign in via main
    func loginWithMail(password : String , email : String,completion : @escaping (Error?)->Void){
        
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            
            if let error = error{
                completion(error)
                return
            }
            
            guard let user = user else{
                completion(error)
                return
            }
            
            self.getFirebaseRefreshedToken(currentUser: user, completion: { (token, providerID) in
                
                guard let token = token,
                    let providerID = providerID else{
                        return
                }
                
                self.saveCurrentUserDataLocally(token: token, providerID: providerID)
                
                self.sendAdditionalUserData(token: token, parameters: ["providerID" : providerID] ){ error in
                    completion(error)
                }
            })
        }
    }
    
    /// Save credential at firebase
    func fireBaseAuth(credential : AuthCredential, completion:@escaping (Error?) -> ()){
        Auth.auth().signIn(with: credential) { [weak self] (user, error) in
            
            if let error = error {
                completion(error)
                return
            }
            
            guard let currentUser = self?.currentUser else{
                completion(error)
                return
            }
            
            self?.getFirebaseRefreshedToken(currentUser: currentUser, completion: { (token, providerID) in
                
                guard let token = token,
                    let providerID = providerID else{
                        completion(TheranicaError.noTokenError)
                        return
                }
                
                self?.saveCurrentUserDataLocally(token: token, providerID: providerID)
                
                self?.sendAdditionalUserData(token: token, parameters: ["providerID" : providerID] ){ error in
                    completion(error)
                }
            })
        }
    }
    
    /// Refresh firebase token
    func getFirebaseRefreshedToken(currentUser : User,
                                   completion : @escaping (_ token : String?, _ providerID : String?)->Void){
        
        currentUser.getIDTokenForcingRefresh(true) {token, error in
            
            guard let token = token,
                let providerID = currentUser.providerData.first?.providerID,
                error == nil else{
                    
                    completion(nil, nil)
                    return
            }
            
            completion(token, providerID)
        }
        
    }
    
    /// Send Additional User Data as providerID, firstName, lastName, etc..
    func sendAdditionalUserData(token : String ,parameters : [String : String] , completion:@escaping (Error?) -> ()){
        
        let token = ["x-auth": token]
        var params = [String : String]()
        
        // add params
        for p in parameters{
            params.updateValue(p.value, forKey: p.key)
        }
        
        Alamofire.request(userRegURL, method: .post, parameters: params
            , encoding: JSONEncoding.default, headers: token).validate().responseJSON { response in
                
                guard let _response = response.response else{
                    completion(response.error)
                    return
                }
                
                if _response.statusCode == 200 {
                    debugPrint("statuscode : \(String(describing: _response.statusCode))")
                    completion(nil)
                } else {
                    debugPrint("statuscodefail : \(String(describing: _response.statusCode))")
                    completion(response.error)
                }
        }
    }
    
    /// Log out
    func logOut(cleanUp : Bool = true, completion :  ((Error?)->Void)? = nil){
        do{
            if cleanUp{
                DBManager.manager.clearContext()
                saveCurrentUserDataLocally(token: nil, providerID: nil)
                DBManager.manager.clearUserDefaults()
            }
            try Auth.auth().signOut()
            completion?(nil)
        }catch{
            completion?(error)
        }
    }
    
    
    
    
    /// Reset password
    func resetPassword(email : String, completion : @escaping (Error?) -> Void ){
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            completion(error)
        }
    }
    
    /// Change Email
    func changeEmail(to email : String, completion : @escaping (Error?)->Void){
        guard let currentUser = currentUser else {
            completion(TheranicaError.noUserConnected)
            return
        }
        currentUser.updateEmail(to: email) {completion($0)}
    }
    
    /// Change Password
    func changePassword(to password : String, completion : @escaping (Error?)->Void){
        guard let currentUser = currentUser else {
            completion(TheranicaError.noUserConnected)
            return
        }
        currentUser.updatePassword(to: password) {completion($0)}
    }
    
    /// Reauthenticate
    func reauthenticate(currentPassword : String, completion : @escaping (Error?) -> Void){
        guard let currentUser = currentUser,
            let email = currentUser.email else {
                completion(TheranicaError.noUserConnected)
                return
        }
        
        let credential = EmailAuthProvider.credential(withEmail: email, password: currentPassword)
        currentUser.reauthenticate(with: credential) {completion($0)}
    }
    
    // Save current user data
    func saveCurrentUserDataLocally(token : String?, providerID : String?){
        UserDefaults.standard.set(token, forKey: Constants.CURRENT_USER_TOKEN)
        UserDefaults.standard.set(providerID, forKey: Constants.CURRENT_PROVIDER_ID)
    }
    
}
extension AuthManager : GIDSignInDelegate{
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        
        if error != nil {
            
            // check if the user has canceled
            let errorStr = error.debugDescription
            if let arrCode = errorStr.components(separatedBy: "Code=").last?.components(separatedBy: " ").first,
                let errorNum = Int(arrCode) {
                
                if errorNum ==  GIDSignInErrorCode.canceled.rawValue{
                    self.delegate?.didFinishGoogleAuth(error: nil)
                    return
                }
                return
            }
            // other errors
            
            self.delegate?.didFinishGoogleAuth(error: error)
            return
        }
        
        guard let authentication = user.authentication else {
            self.delegate?.didFinishGoogleAuth(error: TheranicaError.authenticationError)
            return
        }
        
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        
        
        AuthManager.manager.fireBaseAuth(credential: credential) { error in
            RoutingController.shared.determineRoot()
            
            self.delegate?.didFinishGoogleAuth(error: error)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
}

