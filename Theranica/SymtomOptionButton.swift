//
//  SymtomOptionButton.swift
//  Theranica
//
//  Created by msapps on 27/06/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit

class SymtomOptionButton: UIButton {
    
    override var isSelected: Bool{
        didSet{
            backgroundColor =  isSelected ? Constants.Colors.selectedMenu.value : Constants.Colors.menuCell.value
        }
    }
}
