//
//  Int.swift
//  Theranica
//
//  Created by Maor Shams on 21/02/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import Foundation
extension Int{
    func formattedTime() -> String{
        //   let hours = self / 3600
        let minutes = self / 60 % 60
        let seconds = self % 60
        // return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
        return String(format:"%02i:%02i", minutes, seconds)
    }
}
