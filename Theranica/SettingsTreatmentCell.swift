//
//  SettingsTreatmentCell.swift
//  Theranica
//
//  Created by Maor on 10/08/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit

class SettingsTreatmentCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var myTreatmentView: UIView!
    @IBOutlet weak var treatmentView: UIView!
    

    // Not using isSelected because the twice calling of "isSelcted" bug
    // on collectionview did select method.
    var isPressed : Bool = false{
        didSet{
            UIView.animate(withDuration: 0.2) {
                if self.isPressed{
                    self.myTreatmentView.alpha = 1
                    self.treatmentView.alpha = 0
                }else{
                    self.myTreatmentView.alpha = 0
                    self.treatmentView.alpha = 1
                }
                
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.circled(with: 3)
    }
    
}
