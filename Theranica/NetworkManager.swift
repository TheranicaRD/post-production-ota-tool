//
//  NetworkManager.swift
//  Theranica
//
//  Created by Maor Shams on 21/02/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NetworkManager: NSObject {

    // init
    static let manager = NetworkManager()
    override private init(){}
    
    // Network paths
    
    var baseURL : URL{
        return URL(string: "https://secure-taiga-91414.herokuapp.com")!
    }
    
    var postDiaryURL : URL{
        return baseURL.appendingPathComponent("/postDiary")
    }
    
    var getUserDiariesURL : URL{
        return baseURL.appendingPathComponent("/getUserDiaries")
    }
    
    var updateUserDiaryURL : URL{
        return baseURL.appendingPathComponent("/UpdateUserDiary")
    }
    
    var deleteUserDiaryURL : URL{
        return baseURL.appendingPathComponent("/deleteUserDiary")
    }
    
    
    /// Send HTTP Request to DB.
    /// No need to pass token, if the token is nil, completion with appropriate error
    /// - parameters:
    ///   - url: The URL send to
    ///   - request: The request parameters
    ///   - method:  The HTTPMethod, (By default = .post)
    ///   - completion:  Run after getting results , (By default = nil)
    func sentRequestToDB(url : URL,request : [String : Any]? = nil, method : HTTPMethod = .post, completion : ((Error?,DataResponse<Any>?)->Void)?=nil){
        
        guard let user = AuthManager.manager.currentUser else{
            completion?(TheranicaError.noUserConnected,nil)
            return
        }
        
        guard let token = AuthManager.manager.currentUserToken else{
            completion?(TheranicaError.noTokenError,nil)
            return
        }
        
        guard Reachability.isConnectedToNetwork() else{
            completion?(TheranicaError.noInternetConnection, nil)
            return
        }
        
        let headers = ["x-auth": token]
        
        Alamofire.request(url, method: method, parameters: request, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            guard let statusCode = response.response?.statusCode else{
                completion?(response.error,response)
                return
            }
            
            switch statusCode{
            case 200: break
            case 402:
                
                AuthManager.manager.getFirebaseRefreshedToken(currentUser: user, completion: { (token, providerID) in
                    
                    AuthManager.manager.saveCurrentUserDataLocally(token: token, providerID: providerID)
                    
                    self.sentRequestToDB(url: url, request: request, method: method, completion: { (error, response) in
                        completion?(error,response)
                    })
                    
                })
                return
            default:  completion?(response.error,response)
                return
            }
            
            guard let data = response.data else{
                completion?(response.error,response)
                return
            }
            
            let json = JSON(data: data)
            
            if let nModified = json["nModified"].int{
                completion?(nModified == 1 ? nil : TheranicaError.noValueModified,response)
                return
            }
            
            completion?(json.error,response)
        }
    }
    
    /// READ/WRITE DB METHODS -
    
    /// READ -
    
    /// Fetch User Diaries
    func fetchUserDiaries(completion : @escaping (Error?,[Treatment])->Void){
        
        guard let treatmentDescription = Entity.treatment.description,
            let locationDescription  = Entity.location.description,
            let symptomDescription = Entity.symptom.description else{
                completion(nil, [])
                return
        }
        
        self.sentRequestToDB(url: getUserDiariesURL, method: .get) { (error, response) in
            
            
            if response?.error != nil{
                completion(error, [])
                return
            }
            
            DispatchQueue.global().async{
                
                guard let data = response?.result.value as? NSArray else{
                    completion(nil, [])
                    return
                }
                
                let context = DBManager.manager.context
                
                var treatments : [Treatment] = []
                
                for diary in data{
                    
                    guard let diary = diary as? NSDictionary,
                        let date = ((diary["date"] as? String)?.toDateUTC() as NSDate?),
                        let id = diary["_id"] as? String else{
                            completion(nil, [])
                            continue
                    }
                    
                    
                    let treatment = Treatment(entity: treatmentDescription, insertInto: context)
                    treatment.setValue(date, forKey: "date")
                    treatment.setValue(id, forKey: "id")
                    
                    
                    if let locationObj = diary["location"] as? NSDictionary,
                        let address = locationObj["location"] as? String,
                        let lat = locationObj["lat"] as? Double,
                        let long = locationObj["lng"] as? Double,
                        let weather = locationObj["weather"] as? String{
                        
                        let location = Location(entity: locationDescription, insertInto: context)
                        
                        location.setValue(lat, forKey: "latitude")
                        location.setValue(long, forKey: "longitude")
                        location.setValue(address, forKey: "address")
                        location.setValue(weather, forKey: "weather")
                        
                        // add location to treatment
                        treatment.setValue(location, forKey: "location")
                    }
                    
                    var symptoms : [Symptom] = []
                    
                    if let aura = diary["aura"] as? String{
                        let symptom = Symptom(entity: symptomDescription, insertInto: context)
                        symptom.setValue(SymptomQuestion.aura.title, forKey: Constants.NAME)
                        symptom.setValue(aura, forKey: Constants.PAIN_LEVEL)
                        symptoms.append(symptom)
                        
                    }
                    
                    if let headacheLevel = diary["headacheLevel"] as? String{
                        let symptom = Symptom(entity: symptomDescription, insertInto: context)
                        symptom.setValue(SymptomQuestion.headache_level.title, forKey: Constants.NAME)
                        symptom.setValue(headacheLevel, forKey: Constants.PAIN_LEVEL)
                        symptoms.append(symptom)
                    }
                    
                    if let headacheSide = diary["headacheSide"] as? String{
                        let symptom = Symptom(entity: symptomDescription, insertInto: context)
                        symptom.setValue(SymptomQuestion.headache_side.title, forKey: Constants.NAME)
                        symptom.setValue(headacheSide, forKey: Constants.HEADACHE_SIDE)
                        symptoms.append(symptom)
                    }
                    
                    if let nausea = diary["nausea"] as? String{
                        let symptom = Symptom(entity: symptomDescription, insertInto: context)
                        symptom.setValue(SymptomQuestion.nausea.title, forKey: Constants.NAME)
                        symptom.setValue(nausea, forKey: Constants.PAIN_LEVEL)
                        symptoms.append(symptom)
                    }
                    
                    if let phonophobia = diary["phonophobia"] as? String{
                        let symptom = Symptom(entity: symptomDescription, insertInto: context)
                        symptom.setValue(SymptomQuestion.phonophobia.title, forKey: Constants.NAME)
                        symptom.setValue(phonophobia, forKey: Constants.PAIN_LEVEL)
                        symptoms.append(symptom)
                    }
                    
                    if let photophobia = diary["photophobia"] as? String{
                        let symptom = Symptom(entity: symptomDescription, insertInto: context)
                        symptom.setValue(SymptomQuestion.photophobia.title, forKey: Constants.NAME)
                        symptom.setValue(photophobia, forKey: Constants.PAIN_LEVEL)
                        symptoms.append(symptom)
                    }
                    
                    let set = NSSet(array: symptoms)
                    treatment.setValue(set, forKey: "symptoms")
                    treatments.append(treatment)
                    
                }
                
                completion(nil, treatments)
                
            }
        }
    }
    
    /// WRITE -
    
    /// Delete User Diary
    func deleteUserDiary(treatmentID : String, completion : ((Error?)->Void)?=nil){
        let request = ["id" : treatmentID]
        sentRequestToDB(url: deleteUserDiaryURL, request: request) { (error,_) in
            completion?(error)
        }
    }
    
    /// Update user Diary
    func updateUserDiary(treatmentID : String,symptoms : [Symptom], completion : ((Error?)->Void)?=nil){
        
        var request : [String : Any] = [
            "id" : treatmentID
        ]
        
        // add symptoms
        for symptom in symptoms{
            guard let symptomName = symptom.name else{
                continue
            }
            
            if let headacheSide = symptom.headacheSide{
                request.updateValue(headacheSide, forKey: symptomName.camelcased)
                continue
            }
            
            if let painLevel = symptom.painLevel{
                request.updateValue(painLevel, forKey: symptomName.camelcased)
            }
        }
        
        sentRequestToDB(url: updateUserDiaryURL, request: request) { (error,_) in
            completion?(error)
        }
    }
    
    
    
    ///
    func sendNewDiaryToDB(symptoms : [Symptom], completion : ((Error?)->Void)?=nil){
        
        var request : [String : Any] = [
            "date" : Date().currentDateAndTime
        ]
        
        var locationParams : [String : Any] = [
            "location" : "",
            "weather" : "",
            "lat" : 0.0,
            "lng": 0.0
        ]
        
        // add symptoms
        for symptom in symptoms{
            guard let symptomName = symptom.name else{
                continue
            }
            
            if let headacheSide = symptom.headacheSide{
                request.updateValue(headacheSide, forKey: symptomName.camelcased)
                continue
            }
            
            if let painLevel = symptom.painLevel{
                request.updateValue(painLevel, forKey: symptomName.camelcased)
            }
        }
        
        // get user location
        if let location = LocationManager.manager.getUserLocation(){
            
            // update user location to locationParams
            locationParams.updateValue(location.latitude, forKey: "lat")
            locationParams.updateValue(location.longitude, forKey: "lng")
            
            // get the location in string
            LocationManager.manager.getUserAddress(latitude: location.latitude, longitude: location.longitude, completion: {
                
                // update the string location
                locationParams.updateValue($0, forKey: "location")
                
                // get the current weather
                LocationManager.manager.getCurrentWeather(lat: location.latitude, long: location.longitude){ (weather, error) in
                    
                    // update weather
                    locationParams.updateValue((weather ?? ""), forKey: "weather")
                    // update location object to request
                    request.updateValue(locationParams, forKey: "location")
                    
                    
                    self.sentRequestToDB(url: self.postDiaryURL, request: request,completion: { (error,_) in
                        completion?(error)
                    })
                }
            })
            
        }else{
            self.sentRequestToDB(url: self.postDiaryURL, request: request,completion: { (error,_) in
                completion?(error)
            })
        }
    }
    
    
    func sendSavedDiaryToDB(){
        
        guard Reachability.isConnectedToNetwork() else{
            return
        }
        
        DBManager.manager.fetchCoreDataTreatments { (treatments) in
            
            guard let treatments = treatments else{
                return
            }
            
            
            for treatment in treatments{
                
                guard let userID = treatment.userID,
                    let currentUserID = AuthManager.manager.currentUser?.uid,
                    userID == currentUserID else{
                        continue
                }
                
                guard let diaryRequest = buidDiaryRequest(from: treatment) else{
                    continue
                }
                
                self.sentRequestToDB(url: postDiaryURL, request: diaryRequest, completion: { (error,_) in
                    
                    // if there is error, return
                    guard error == nil else{
                        return
                    }
                    
                    // else delete from core data
                    DBManager.manager.deleteDiaryFromCoreData(treatment)
                    
                })
            }
        }
    }
    
    
    private func buidDiaryRequest(from treatment : Treatment) -> [String : Any]?{
        
        guard let date = treatment.date as Date? else{
            return nil
        }
        
        var request : [String : Any] = [
            "date" : date.currentDateAndTime
        ]
        
        var locationParams : [String : Any] = [:]
        
        // add symptoms
        if let symptoms = treatment.symptoms{
            
            for symptom in symptoms{
                
                guard let symptom = symptom as? Symptom else{
                    continue
                }
                
                guard let symptomName = symptom.name else{
                    continue
                }
                
                if let headacheSide = symptom.headacheSide{
                    request.updateValue(headacheSide, forKey: symptomName.camelcased)
                    continue
                }
                
                if let painLevel = symptom.painLevel{
                    request.updateValue(painLevel, forKey: symptomName.camelcased)
                }
            }
        }
        
        if let locationObj = treatment.location{
            
            if let address = locationObj.address{
                locationParams.updateValue(address, forKey: "location")
            }
            
            if let weather = locationObj.weather{
                locationParams.updateValue(weather, forKey: "weather")
            }
            
            locationParams.updateValue(locationObj.latitude, forKey: "lat")
            locationParams.updateValue(locationObj.longitude, forKey: "lng")
        }
        
        return request
    }
    
}
