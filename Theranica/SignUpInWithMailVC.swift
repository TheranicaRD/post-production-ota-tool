//
//  LogInWithMailVC.swift
//  Theranica
//
//  Created by Roei Baruch on 20/08/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit
import Alamofire

class SignUpInWithMailVC: UIViewController  {
    
    @IBOutlet weak var emailTextField: BottomLineTextField!
    @IBOutlet weak var passwordTextField: BottomLineTextField!
    @IBOutlet weak var repeatPasswordTextField: BottomLineTextField!
    @IBOutlet weak var firstNameField: BottomLineTextField!
    @IBOutlet weak var lastName: BottomLineTextField!
    @IBOutlet weak var nextBtn: UIButton!
    
    var currentTextField : UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpToolbar(textField: emailTextField)
        self.setUpToolbar(textField: passwordTextField)
        self.setUpToolbar(textField: firstNameField)
        self.setUpToolbar(textField: lastName)
        
        self.nextBtn.isUserInteractionEnabled = false
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SignUpInWithMailVC.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
        
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func textFieldDidChanged(_ sender: Any) {
        let validation = generalValidation()
        if validation {
            self.nextBtn.backgroundColor = UIColor(red:0.03, green:0.41, blue:0.6, alpha:1.0)
            
        }else{
            self.nextBtn.backgroundColor =  UIColor(white:0.7, alpha:1.0)
        }
    }
    func isPasswordMatch(password : String) -> Bool {
        if  password == self.passwordTextField.text {
            return true
        }else{
            return false
        }
    }
    
    func generalValidation() -> Bool {
        guard let passwordField = self.passwordTextField.text,
            let firstNameField = self.firstNameField.text,
            let emailField = self.emailTextField.text,
            let lastNameField = self.lastName.text,
            let repeatPassword = self.repeatPasswordTextField.text else{
                return false
        }
        
        let isEmailAddressValid = Utils.isValidEmail(emailField)
        
        let isPasswordValidate = Utils.isPasswordValid(passwordField)
        
        let isPasswordMatching = isPasswordMatch(password: repeatPassword)
        
        let isFirstNameValid = isValidName(name: firstNameField)
        
        let isLastNameValid = isValidName(name: lastNameField)
        
        
        if (isEmailAddressValid && isPasswordValidate && isPasswordMatching && isFirstNameValid && isLastNameValid ){
            self.nextBtn.isUserInteractionEnabled = true
            return true
        }else{
            self.nextBtn.isUserInteractionEnabled = false
            return false
        }
        
    }
    
    func isValidName(name : String) -> Bool{
        return name.count >= 2 && !name.contains(" ")
    }
    
    @IBAction func nextBtn(_ sender: Any) {
        
        guard let password = self.passwordTextField.text,
            let firstName = self.firstNameField.text,
            let email = self.emailTextField.text,
            let lastName = self.lastName.text else{
                return
        }
        
        let loadingAlert = Utils.alert(title: "Verifying...", message : "Please wait while connecting" , type: .wait ,autoHide : false)
        
        AuthManager.manager.requestMailAuth(password: password, firstName: firstName, lastName: lastName, email: email) { (error) in
            if let error = error {
                loadingAlert.close()
                _ = Utils.alert(title: Constants.ERROR,message: error.localizedDescription, type: .error)
                return
            }
            RoutingController.shared.determineRoot()
        }
        
        
    }
    
    @IBAction func nextTextField(_ sender: UITextField) {
        let NextTag = currentTextField.tag+1
        
        let nextField : UIResponder? = (currentTextField.superview?.viewWithTag(NextTag))
        
        if let field = nextField{
            field.becomeFirstResponder()
        }else{
            currentTextField.becomeFirstResponder()
        }
    }
    
    @IBAction func resetPasswordAction(_ sender: UIButton) {
        if let nextVC = storyboard?.instantiateViewController(withIdentifier: "resetPasswordVC"){
        self.navigationController?.pushViewController(nextVC, animated: true)
        }
    }
    
    
    
}

extension SignUpInWithMailVC : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        currentTextField = textField
        return true
    }
    
    func setUpToolbar(textField: UITextField) {
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        toolBar.barStyle = UIBarStyle.default
        toolBar.items = [
            UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.plain, target: self, action:#selector(nextTextField(_:))),
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
        ]
        toolBar.sizeToFit()
        textField.inputAccessoryView = toolBar
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let passwordField = self.passwordTextField.text,
            let firstNameField = self.firstNameField.text ,
            let emailField = self.emailTextField.text,
            let lastNameField = self.lastName.text ,
            let repetPasswordField = self.repeatPasswordTextField.text else{
                return
        }
        
        guard let textField = textField as? BottomLineTextField else{
            return
        }
        
        if textField == self.emailTextField {
            let isEmailAddressValid = Utils.isValidEmail(emailField)
            textField.hasError = !isEmailAddressValid
            return
        }else if textField == self.lastName {
            let isLastNameValid = isValidName(name: lastNameField)
            textField.hasError = !isLastNameValid
            return
        }else if textField == self.firstNameField {
            let isFirstNameValid = isValidName(name: firstNameField)
            textField.hasError = !isFirstNameValid
            return
        }else if textField == self.passwordTextField {
            let isPasswordValidate = Utils.isPasswordValid(passwordField)
            textField.hasError = !isPasswordValidate
            return
        }else if textField == self.repeatPasswordTextField {
            let isPasswordMatching = isPasswordMatch(password: repetPasswordField)
            textField.hasError = !isPasswordMatching
            return
        }
    }
}

