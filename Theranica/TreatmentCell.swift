//
//  TreatmentCell.swift
//  Theranika
//
//  Created by msapps on 18/06/2017.
//  Copyright © 2017 msapps. All rights reserved.
//

import UIKit

class TreatmentCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var myTreatmentView: UIView!
    @IBOutlet weak var treatmentView: UIView!
    
    
    // Not using isSelected because the twice calling of "isSelcted" bug
    // on collectionview did select method.
    var isPressed : Bool = false{
        didSet{
            if isPressed{
                if isMyTreatment{
                    contentView.backgroundColor = Constants.Colors.settingsSelectedTreatmentCell.value
                    myTreatmentView.alpha = 1
                    treatmentView.alpha = 0
                }else{
                    contentView.backgroundColor = Constants.Colors.selectedMenu.value
                }
                  imageView.image = #imageLiteral(resourceName: "image_stop")
                
            }else{
                if !isMyTreatment{
                      contentView.backgroundColor =  Constants.Colors.menuCell.value
                }
                imageView.image = #imageLiteral(resourceName: "image_start")
            }
            
        }
    }
    
    var isMyTreatment : Bool = false{
        didSet{
            if isMyTreatment{
                contentView.backgroundColor = Constants.Colors.settingsSelectedTreatmentCell.value
                treatmentView.alpha = 0
                myTreatmentView.alpha = 1
                
            }else{
                contentView.backgroundColor =  Constants.Colors.menuCell.value
                myTreatmentView.alpha = 0
                treatmentView.alpha = 1
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.circled(with: 3)
    }
    
}
