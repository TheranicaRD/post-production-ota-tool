//
//  MigraineEducationToolboxVC.swift
//  Theranica
//
//  Created by msapps on 29/06/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit

class MigraineEducationVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    typealias MigraineEducationMenuItem = (text : String, image : UIImage, url : String, isVideo : Bool)
    
    let menuItems : [MigraineEducationMenuItem] = [
        ("What Causes Migraines?",#imageLiteral(resourceName: "image_video"),"RJeqcikMKAI",true),
        ("Headache Relief Guide",#imageLiteral(resourceName: "image_read"),"http://www.headachereliefguide.com/learn.php",false)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideBackButton()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nextVC = segue.destination as? WebviewVC, let indexPath = sender as? IndexPath{
            let item = menuItems[indexPath.section]
             nextVC.headTitle = item.text
            if item.isVideo{
                nextVC.videoCode = item.url
            }else{
                nextVC.url = item.url
            }
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}
extension MigraineEducationVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CELL, for: indexPath) as! MigraineEducationCell
        cell.iconImageView.image = menuItems[indexPath.section].image
        cell.titleLabel.text = menuItems[indexPath.section].text
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "MigraineEducationToWebView", sender: indexPath)
        
    }
    
    
}
