//
//  MigraineDiaryVC.swift
//  Theranika
//
//  Created by msapps on 19/06/2017.
//  Copyright © 2017 msapps. All rights reserved.
//

import UIKit
import CoreLocation
class MigraineDiaryVC: UIViewController {
    
    var treatments = [Treatment]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.hideBackButton()
        
        fetchData()
        
    }
    
    deinit {
        DBManager.manager.clearContext()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let cell = sender as? MigraineDiaryCell,
            let indexPath = tableView.indexPath(for: cell),
            let nextVC = segue.destination as? DetailsMigraineDiaryVC{
            let treatment = treatments[indexPath.row]
            nextVC.treatment = treatment
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
    }
    
    func fetchData(){
        // core data
        //         DBManager.manager.fetchCoreDataTreatments { (treatments) in
        //         if let treatments = treatments {
        //         self.treatments = treatments
        //         }
        //         }
        
        if Reachability.isConnectedToNetwork(){
            
            let loadingAnmation = Utils.alert(title: "Loading", message: "Please wait...", type: .wait, autoHide: false)
            
            NetworkManager.manager.fetchUserDiaries{ error,treatments in
                
                DispatchQueue.main.async{
                    loadingAnmation.close()
                    if let error = error{
                        _ = Utils.alert(title: Constants.ERROR, message: error.localizedDescription, type: .error)
                    }else{
                        self.treatments = treatments
                        self.tableView.reloadData()
                    }
                }
            }
        }else{
            _ = Utils.alert(title: "Error", message: "No Internet connection, Please try again ..", type: .error, showCloseButton: true, autoHide: false)
        }
    }
}
extension MigraineDiaryVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return treatments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:Constants.CELL) as! MigraineDiaryCell
        
        if let date = treatments[indexPath.row].date{
            cell.dateLabel.text = Utils.migraneDiaryFormat(date: date as Date)
        }
        
        if let address = treatments[indexPath.row].location?.address{
            cell.locationLabel.text = address
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            if Reachability.isConnectedToNetwork(){
                guard let treatmentID = treatments[indexPath.row].id else{
                    return
                }
                tableView.beginUpdates()
                self.treatments.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .automatic)
                tableView.endUpdates()
                NetworkManager.manager.deleteUserDiary(treatmentID: treatmentID)
            }else{
                tableView.setEditing(false, animated: true)
                _ = Utils.alert(title: "Error",message: "No Interner connection, Please try again..", type: .error)
            }
        }
    }
    
    
}
