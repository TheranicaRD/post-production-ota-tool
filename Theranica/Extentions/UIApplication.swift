//
//  UIApplication.swift
//  Theranica
//
//  Created by Maor Shams on 21/02/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit
extension UIApplication{
    var mostTopVC : UIViewController?{
        if var topController = keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return nil
    }
}
