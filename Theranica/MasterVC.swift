//
//  ViewController.swift
//  Theranika
//
//  Created by msapps on 13/06/2017.
//  Copyright © 2017 msapps. All rights reserved.
//

import UIKit
import TheranicaBLE

class MasterVC: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var rightBottomView: UIView!
    @IBOutlet weak var leftBottomView: UIView!
    
    @IBOutlet weak var patchConnectionStatus: UILabel!
    @IBOutlet weak var patchBatteryLevel: UILabel!
    
    lazy var menuVC : UINavigationController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var vc = storyboard.instantiateViewController(withIdentifier: "MenuVC") as! UINavigationController
        return vc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
        observeNotificaton(tNotification: .batteryLevelChange)
        observeNotificaton(tNotification: .connectionStatusChange)
        observeNotificaton(tNotification: .bluetoothPowerDidChange)
    }
    
    func observeNotificaton(tNotification : TDeviceNotification){
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: tNotification.name), object: nil, queue: nil) {
            let info = $0.userInfo?[tNotification.info]
            
            DispatchQueue.main.async {
                
                if tNotification == .batteryLevelChange, let info = info as? Int{
                    self.batteryLevelDidChanged(to: info)
                }else if tNotification == .connectionStatusChange,let info = info as? BLConnectionState{
                    self.deviceConnectionStatusChange(to: info)
                }else if  tNotification == .bluetoothPowerDidChange ,let isOn = info as? Bool{
                    if !isOn{
                        self.deviceConnectionStatusChange(to: .disconnected, alert : false)
                        self.batteryLevelDidChanged(to: -1)
                    }
                }
            }
        }
    }
    
    func batteryLevelDidChanged(to level : Int){
        UIView.animate(withDuration: 0.3) {
            var text = "\(level)%"
            if level == -1 { text = "N/A" }
            self.patchBatteryLevel.text = text
        }
    }
    
    func deviceConnectionStatusChange(to state : BLConnectionState, alert : Bool = true){
        UIView.animate(withDuration: 0.3) {
            
            let  patchConnected = "device_connected".localized
            
            let isConnected = state == .connected ? "yes".localized : "no".localized
            
            self.patchConnectionStatus.text = patchConnected + " " + isConnected
            
        }
        if alert{
            _ = Utils.alert(title: state.value.localized, type: .info)

        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Utils.setStatusBarColor()
        
        
    }
    
    func setupView(){
        self.addViewControllerAs(child: menuVC)
        Utils.setStatusBarColor()
    }
    
    func addViewControllerAs(child : UIViewController){
        self.addChildViewController(child)
        self.containerView.addSubview(child.view)
        child.view.frame = containerView.bounds
        child.view.autoresizingMask = [.flexibleWidth , .flexibleHeight]
        child.didMove(toParentViewController: self)
    }
}
