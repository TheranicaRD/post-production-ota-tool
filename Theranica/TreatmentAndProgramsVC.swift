//
//  TreatmentAndProgramsVC.swift
//  Theranica
//
//  Created by Maor on 10/08/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit
import TGPControls

class TreatmentAndProgramsVC: UIViewController {
    
    @IBOutlet weak var sliderLabels: TGPCamelLabels!
    @IBOutlet weak var slider: TGPDiscreteSlider!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var buttonsArray : [TreatmentProgram.ProgramType] = []
    var selectedProgramHashValue : Int?
    
    var sliderValue : String{
        let index = Int(slider.value)
        return sliderLabels.names[index]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSlider()
        
        buttonsArray = Utils.iterateEnum(TreatmentProgram.ProgramType.self).map{
            return $0
        }
        
        selectedProgramHashValue = getSavedProgramHashValue()
    }
    
    func setupSlider(){
        slider.ticksListener = sliderLabels
        sliderLabels.names = ["20","25","30","35","40","45"]
        slider.addTarget(self, action: #selector(sliderValueDidChange(sender:)), for: .valueChanged)
        
        if let sliderValue = getSliderValue(),
            let index = sliderLabels.names.index(of: sliderValue){
            slider.value = CGFloat(index)
            sliderLabels.value = UInt(index)
        }
    }
    
    
    // called once the slider did change
    @objc func sliderValueDidChange(sender : TGPDiscreteSlider){
        saveSliderValue()
    }
    
    // select/deselect cells except cell
    func cellsIsPressed(isPressed : Bool , exceptCell : SettingsTreatmentCell){
        guard let cells = collectionView.visibleCells as? [SettingsTreatmentCell] else{
            return
        }
        
        _ = cells.map { if $0 != exceptCell { $0.isPressed = isPressed } }
    }
    
    // get the saved hash value of selected program
    // if there is no selected program return 0
    func getSavedProgramHashValue() -> Int{
        return UserDefaults.standard.integer(forKey: Constants.MY_TREATMENT_PROGRAM)
    }
    
    // save the hash value of selected program
    func saveProgramHashValue(_ programHashValue : Int){
        UserDefaults.standard.set(programHashValue, forKey: Constants.MY_TREATMENT_PROGRAM)
    }
    
    // save the slider value
    func saveSliderValue(){
        UserDefaults.standard.set(sliderValue, forKey: Constants.MY_TREATMENT_PROGRAM_TIME)
    }
    // get the slider value
    func getSliderValue() -> String?{
        return UserDefaults.standard.string(forKey: Constants.MY_TREATMENT_PROGRAM_TIME)
    }

}
extension TreatmentAndProgramsVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return buttonsArray.count - 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CELL, for: indexPath) as! SettingsTreatmentCell
        
        let tratmentProgram = buttonsArray[indexPath.item]
        cell.titleLabel.text = tratmentProgram.title
        
    
        // if there is saved program, select it
        if let selectedProgramHashValue = selectedProgramHashValue,
            selectedProgramHashValue == tratmentProgram.hashValue {
            cell.isPressed = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! SettingsTreatmentCell
        
        let programHashValue = buttonsArray[indexPath.row].hashValue
        saveProgramHashValue(programHashValue)
        selectedProgramHashValue = programHashValue
        
        cellsIsPressed(isPressed: false, exceptCell: cell)
        if !cell.isPressed{ cell.isPressed = true }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width  = (collectionView.bounds.size.width / 2) - 4
        let height = (collectionView.bounds.size.height / 3) - 4
        return CGSize(width: width, height: height)
    }
}
