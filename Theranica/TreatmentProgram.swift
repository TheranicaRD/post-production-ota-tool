//
//  TreatmentProgram.swift
//  Theranica
//
//  Created by Maor Shams on 24/08/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit

class TreatmentProgram : NSObject{
    
    enum ProgramType{
        case program1
        case program2
        case program3
        case program4
        case program5
        case program6
        case `default`
        
        var title : String{
            switch self {
            case .program1 : return "treat1".localized
            case .program2 : return "treat2".localized
            case .program3 : return "treat3".localized
            case .program4 : return "treat4".localized
            case .program5 : return "treat5".localized
            case .program6 : return "treat6".localized
            case .default  : return "my_program".localized
            }
        }
        
        static func getType(from hash : Int) -> ProgramType?{
            switch hash {
            case 0: return .program1
            case 1: return .program2
            case 2: return .program3
            case 3: return .program4
            case 4: return .program5
            case 5: return .program6
            case 6: return .default
            default: return nil
                
            }
        }
    }
    
    
    let type : ProgramType
    var intensity : Int? = nil
    var totalTime   : Int? = nil
    
    
    init(type : ProgramType,totalTime : Int? = nil, intensity : Int? = nil) {
        self.type = type
        self.totalTime = totalTime
         self.intensity = intensity
    }
}

