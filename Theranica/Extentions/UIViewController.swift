//
//  UIViewController.swift
//  Theranica
//
//  Created by Maor Shams on 21/02/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit
extension UIViewController{
    func hideBackButton(){
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}
