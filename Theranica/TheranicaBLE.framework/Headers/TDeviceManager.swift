//
//  BleManager.swift
//  Theranica
//
//  Created by Maor on 15/08/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit
import CoreBluetooth

open class TDeviceManager: NSObject {
    
    // Init
    open static let manager = TDeviceManager()
    override private init(){}

    /// Init the library, must be called at the first time accesing library
    open func initLibrary(with delegateTarget: TDeviceManagerDelegate,
                          mode : TLibraryMode,
                          uniqueIdentifier : String){
        delegate = delegateTarget
        self.libraryMode = mode
        bleManager = CBCentralManager(delegate: self, queue: .main, options: nil)
        self.uniqueIdentifier = uniqueIdentifier
    }
    
    open var bleManager : CBCentralManager!
    open var delegate : TDeviceManagerDelegate!
    open var libraryMode : TLibraryMode!
    
    // Core Bluetooth
    fileprivate var peripheral : CBPeripheral?
    fileprivate var peripherals = [CBPeripheral]()
    fileprivate var characteristic : CBCharacteristic?
    
    open var uniqueIdentifier : String!
    
    // Peripheral data
    fileprivate let serviceUUID        = "71387664-eb78-11e6-b006-92361f002671"
    fileprivate let characteristicUUID = "71387663-eb78-11e6-b006-92361f002671"
    
    // Last status get
    private var lastStatusGet : StatusGet?
    
    open func getLastStatusGet() -> StatusGet?{
        return lastStatusGet
    }
    
    /// Check if device is connected to BLE
    open var isBLEConnected : Bool{
        if let peripheral = peripheral{
            return peripheral.state == .connected
        }
        return false
    }
    
    /// Check if bluetooth is On
    open  var isBluetoothOn : Bool{
        return bleManager?.state == .poweredOn
    }
    
    func sendUDID(){
        guard let udid = uniqueIdentifier,
            let fixedUdid = get14Chars(from: udid) else{
                return
        }
        
        if  let data = "UDID \(fixedUdid)\r".utfToHex(){
            
            self.writeToPeripheral(data : data)
        }
    }
    
    func get14Chars(from string : String) -> String?{
        guard string.count >= 14 else{
            return nil
        }
        
        let tempStr = string.replacingOccurrences(of: "-", with: "")
        var newStr = ""
        
        for char in tempStr{
            if newStr.count == 14 { return newStr }
            newStr.append(char)
        }
        
        return nil
    }
    
    /// Scan for peripherals
    open  func startScan(){
        self.bleManager?.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey : false] )
    }
    
    func saveUUID(uuid : String?){
        UserDefaults.standard.set(uuid, forKey: "uuid")
    }
    
    open func cleanSavedData(){
        self.saveUUID(uuid : nil)
        self.lastStatusGet = nil
        self.disconnect()
    }
    
    open var uuid : UUID?{
        return UUID(uuidString: UserDefaults.standard.string(forKey: "uuid") ?? "")
    }
    
    /// Stop scan for peripherals
    open  func stopScan(){
        bleManager?.stopScan()
    }
    
    
    /// Connect to peripheral
    /// - parameters:
    ///   - peripheralMac: the mac address of peripheral
    open  func connectToPeripheral(macAddress : String,withAttempts : Int = 1 ,completion  : @escaping (TError?) -> Void){
        
        startScan()
        
        DispatchQueue.global().async{
            var counter = 1
            
            func tryRunning(){
                
                guard counter <= withAttempts else {
                    completion(TError.deviceNotFound)
                    return
                }
                
                counter += 1
                
                let filteredPeripherals = self.peripherals.filter({ peripheral in
                    if let name = peripheral.name, let hex = name.peripheralName , hex.contains(macAddress){
                        
                        return true
                    }
                    return false
                })
                
                
                
                if let firstFiltered  = filteredPeripherals.first{
                    self.bleManager?.connect(firstFiltered, options: nil)
                    completion(nil)
                    self.stopScan()
                    return
                }
                
                
                DispatchQueue.global().asyncAfter(deadline: .now() + 1, execute: {
                    tryRunning()
                })
                
            }
            
            tryRunning()
        }
    }
    
    /// Connect to peripheral
    /// - parameters:
    ///   - uuid: the uuid of peripheral
    open  func connectToPeripheral(uuid : UUID,withAttempts : UInt = 1){
        
        if let peripheral = peripheral{
            self.bleManager?.connect(peripheral, options: nil)
            return
        }
        
        startScan()
        
        DispatchQueue.global().async{
            var counter = 1
            
            func tryRunning(){
                
                
                
                guard counter <= withAttempts else {
                    return
                }
                
                counter += 1
                
                let filteredPeripherals = self.peripherals.filter({ peripheral in
                    return peripheral.identifier.uuidString == uuid.uuidString ? true : false
                })
                
                
                
                if let firstFiltered  = filteredPeripherals.first{
                    self.bleManager?.connect(firstFiltered, options: nil)
                    self.stopScan()
                    return
                }
                
                
                DispatchQueue.global().asyncAfter(deadline: .now() + 1, execute: {
                    tryRunning()
                })
                
            }
            
            tryRunning()
        }
    }
    
    /// Disconnect from peripheral
    open  func disconnect(){
        if let peripheral = peripheral{
            bleManager?.cancelPeripheralConnection(peripheral)
            self.peripheral = nil
            
        }
    }
    
    /// Send version request from device
    open func sendVersion(){
        if  let data = "VERSION\r\n".utfToHex(){
            writeToPeripheral(data: data)
        }
    }
    
    func sendNotification(tNotification : TDeviceNotification, info : Any){
        let notification = Notification(name: Notification.Name(tNotification.name), object: nil, userInfo: [tNotification.info : info])
        NotificationCenter.default.post(notification)
    }
    
    /// Update patch status
    /// - parameters:
    ///   - state: The state of the patch
    func updatePatchConnectionStatus(to state : BLConnectionState){
        self.delegate?.tDeviceConnectionStatusDidChange(to: state)
        sendNotification(tNotification: .connectionStatusChange, info: state)
    }
    
    /// Update patch battery Level
    /// - parameters:
    ///   - level: The level of battery
    func updateBatteryLevel(to level : Int, statusGet : StatusGet){
        self.delegate?.tDeviceBatteryLevelDidChange(to: level, statusGet : statusGet)
        sendNotification(tNotification: .batteryLevelChange, info: level)
    }
    
    /// Write data to peripheral
    open func writeToPeripheral(data : Data){
        guard let peripheral = peripheral,
            let characteristic = characteristic else{
                return
        }
        peripheral.writeValue(data, for: characteristic, type: .withResponse)
    }
    
    open func stopTreatment(){
        if  let data = "STATUS STOP\r\n".utfToHex(){
            self.writeToPeripheral(data: data)
        }
        if  let data = "CLEAN\r".utfToHex(){
            self.writeToPeripheral(data: data)
        }
    }
    
    open func setIntensity(levelInPercentage : Double){
        let level = BleUtils.convertAppToDeviceIntensity(from: levelInPercentage)
        if  let data = "AMPLITUDE \(level))\r\n".utfToHex(){
            self.writeToPeripheral(data: data)
        }
    }
    
    open func getStatus(){
        if let data = "STATUS GET\r\n".utfToHex(){
            self.writeToPeripheral(data: data)
        }
    }
    
    // On Recive Status get :
    func statusGetRecived(values : String){
        
        // split the values to array
        let components : [String] = values.components(separatedBy: " ")
        
        // try creating object from response
        guard let statusGet = try? StatusGet(components: components) else{
            debugPrint("Status Get recived error")
            return
        }
        
        
        if statusGet.programState != lastStatusGet?.programState{
            sendNotification(tNotification: .programStateDidChange, info: statusGet.programState)
        }
        
        // notify delegates
        self.delegate?.tDeviceDidRecivedStatusGet(statusGet)
        
        // update battery level
        self.updateBatteryLevel(to: statusGet.batteryLevel, statusGet : statusGet)
        
        
        if let lastStatusGet = self.lastStatusGet {
            
            // Check for last status, if is .notRunning -> Start treatment program
            if (lastStatusGet.programState != .running &&
                statusGet.programState == .running){
                
                self.delegate?.tDeviceDidStartTreatment(statusGet)
            }
            
            if lastStatusGet.programState == .running {
                
                if statusGet.programState != .running{
                    
                    debugPrint(lastStatusGet.elapsedTime)
                    
                    if libraryMode == .testing{
                        if lastStatusGet.elapsedTime >= (30 * 60){
                            self.delegate?.tDeviceDidFinishedTreatment(statusGet: statusGet)
                        }else{
                            self.delegate?.tDeviceDidStoppedTreatment(statusGet : statusGet)
                        }
                    }else{
                        self.delegate?.tDeviceDidFinishedTreatment(statusGet: statusGet)
                    }
                }
                
            }
        }
        
        if statusGet.programState == .running{
            
            let currentAmp = statusGet.amplitude
            let lastAmp = lastStatusGet?.amplitude
            
            if currentAmp != lastAmp{
                self.delegate?.tDeviceAmplitudeDidChange(to: currentAmp, statusGet : statusGet)
            }
            
            if statusGet.elapsedTime >= statusGet.totalTime{
                self.delegate?.tDeviceDidFinishedTreatment(statusGet : statusGet)
            }
        }
        
        // save current status
        self.lastStatusGet = statusGet
        
        
    }
    
    func didDiscoverCharacteristic( characteristic : CBCharacteristic){
        self.characteristic = characteristic
        self.sendUDID()
        self.delegate?.peripheralDidDiscover?(characteristic: characteristic)
        
    }
    
}

extension TDeviceManager : CBCentralManagerDelegate{
    
    public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        
        self.updatePatchConnectionStatus(to: .connected)
        self.peripheral = peripheral
        peripheral.delegate = self
        peripheral.discoverServices(nil)
        
        saveUUID(uuid: peripheral.identifier.uuidString)
        stopScan()
        
    }
    
    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        
        /* michelle told to delete 31.12.16
         if lastStatusGet?.programState == .running {
         self.delegate?.tDeviceDidStoppedTreatment(statusGet : lastStatusGet)
         //
         }*/
        
        
        updatePatchConnectionStatus(to: .disconnected)
        if let lastStatusGet = lastStatusGet {
            updateBatteryLevel(to: -1,statusGet : lastStatusGet)
        }
        
        if let index = peripherals.index(of: peripheral){
            peripherals.remove(at: index)
        }
        self.lastStatusGet = nil
    }
    
    public func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        updatePatchConnectionStatus(to: .disconnected)
    }
    
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        self.delegate?.peripheralDidDiscover?(peripheral: peripheral)
        if !peripherals.contains(where: { return $0.identifier == peripheral.identifier }){
            peripherals.append(peripheral)
        }
    }
    
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state.rawValue{
        case 4: // poweredOff:
            self.delegate?.bluetoothPowerDidChange(isOn: false)
            self.sendNotification(tNotification: .bluetoothPowerDidChange, info: false)
        case 5: //poweredOn:
            self.delegate?.bluetoothPowerDidChange(isOn: true)
            self.sendNotification(tNotification: .bluetoothPowerDidChange, info: true)
        default:
            return
        }
    }
}
extension TDeviceManager : CBPeripheralDelegate{
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?){
        guard let services = peripheral.services else{
            return
        }
        
        services.filter { $0.uuid.uuidString.lowercased() == self.serviceUUID }.forEach {
             peripheral.discoverCharacteristics(nil, for: $0)
        }
        
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?){
        guard let characteristics = service.characteristics else{
            return
        }
        
        characteristics.filter { $0.uuid.uuidString.lowercased() == characteristicUUID }.forEach {
            self.didDiscoverCharacteristic(characteristic: $0)
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?){
        self.delegate?.peripheralDidUpdateValue?(for : characteristic)
        
        if let status = characteristic.value?.bytes.data?.hexToUtf(){
            
            
            let components : [String] = status.components(separatedBy: " ")
            if components.count >= 6{
                statusGetRecived(values: status)
                return
            }
            
            if status.contains("Version"){
                self.delegate?.tDeviceDidRecivedVersion(version: status)
            }
            
            
        }else{
            // let str =  String(data: characteristic.value!, encoding: String.Encoding.utf8)
            
        }
        
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?){
        peripheral.readValue(for: characteristic)
        
    }
    
}


