//
//  ResetPasswordVC.swift
//  Theranica
//
//  Created by Maor Shams on 05/09/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit

class ResetPasswordVC: UIViewController {
    
    @IBOutlet weak var emailTextfield: BottomLineTextField!
    @IBOutlet weak var resetPasswordButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ResetPasswordVC.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ResetPasswordVC.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @IBAction func emailTextfieldDidChange(_ sender: BottomLineTextField) {
        
        guard let email = sender.text else{
            return
        }
        
        if Utils.isValidEmail(email){
            resetPasswordButton.backgroundColor =  UIColor(red:0.03, green:0.41, blue:0.6, alpha:1.0)
            sender.hasError = false
        }else{
            resetPasswordButton.backgroundColor =  UIColor(white:0.7, alpha:1.0)
            sender.hasError = true
        }
    }
    
    @IBAction func emailTextfieldDidEndOnExit(_ sender: BottomLineTextField) {
        sender.resignFirstResponder()
    }
    
    @IBAction func resetPasswordAction(_ sender: UIButton) {
        
        guard let email = emailTextfield.text else{
            return
        }
        
        let loadingAnimation = Utils.alert(title: "Loading", message: "Please wait..", type: .wait, autoHide: false)
        
        AuthManager.manager.resetPassword(email: email) { (error) in
            loadingAnimation.close()
            
            if let error = error{
                _ = Utils.alert(title: Constants.ERROR, message: error.localizedDescription, type: .error, showCloseButton: true, autoHide: false)
                return
            }
            
            _ = Utils.alert(title: "Success", message: "Please check your email..", type: .success, showCloseButton: true, autoHide: false, completion: { 
                self.navigationController?.popViewController(animated: true)
            })
        }
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
}
