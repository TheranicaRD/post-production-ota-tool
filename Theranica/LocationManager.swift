//
//  LocationManager.swift
//  Theranica
//
//  Created by Maor on 09/07/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//
import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON

class LocationManager: NSObject {
    
    static let manager = LocationManager()
    override private init() {}
    let locationManager = CLLocationManager()
    var currentLocation : CLLocation?
    
    typealias JSONDictionary = [String:Any]
    
    /// Request location auth for foreground usage
    func requestAuth(){
        self.locationManager.requestWhenInUseAuthorization()
    }
    
    /// Start observe user location
    func fetchUserLocation(){
        guard CLLocationManager.locationServicesEnabled() else{
            return
        }
        currentLocation = locationManager.location
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.startUpdatingLocation()
    }
    
    /// Stop observe user location
    func stopFetchingUserLocation(){
        locationManager.delegate = nil
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.stopUpdatingLocation()
        locationManager.stopMonitoringSignificantLocationChanges()
    }
    
    /// Get user addresses
    /// - parameters:
    ///   - completion: Dictionary of addresses
    func getUserAddresses(with completion : @escaping (JSONDictionary?) -> Void){
        
        guard let currentLocation = self.currentLocation,
            CLLocationManager.locationServicesEnabled() else {
                return
        }
        
        CLGeocoder().reverseGeocodeLocation(currentLocation) { (placemarks, error) in
            
            if let error = error{
                debugPrint(error)
                completion(nil)
                return
            }
            
            guard let placeMark = placemarks?[0],
                let address = placeMark.addressDictionary as? JSONDictionary? else{
                    return
            }
            
            completion(address)
        }
        
    }
    
    /// Convert user latitude,longitude to string representation
    /// - parameters:
    ///   - location: Core Data location object
    ///   - completion: String representation of Country and City.
    func getUserAddress(from location : Location, completion : @escaping (String) -> Void){
        
        let latitude : Double = location.latitude
        let longitude : Double = location.longitude
        
        let cLLocation = CLLocation(latitude: latitude, longitude: longitude)
        
        CLGeocoder().reverseGeocodeLocation(cLLocation) { (placemarks, error) in
            
            // If there is no location return "Unknown"
            
            if let error = error{
                debugPrint(error)
                completion("Unknown")
                return
            }
            
            guard let placeMark = placemarks?[0],
                  let addresses = placeMark.addressDictionary as? JSONDictionary?,
                  let city = addresses?["City"] as? String,
                  let country = addresses?["Country"] as? String else {
                    
                    completion("Unknown")
                    return
            }
            
            let address = "\(city), \(country)"
            completion(address)
            
        }
    }
    
    
    /// Get the String location from lat, long, If there is no location return ""
    func getUserAddress(latitude : Double, longitude : Double, completion : @escaping (String) -> Void){
        
        let cLLocation = CLLocation(latitude: latitude, longitude: longitude)
        
        CLGeocoder().reverseGeocodeLocation(cLLocation) { (placemarks, error) in
            
            if let error = error{
                debugPrint(error)
                completion("Unknown")
                return
            }
            
            guard let placeMark = placemarks?[0],
                let addresses = placeMark.addressDictionary as? JSONDictionary?,
                let city = addresses?["City"] as? String,
                let country = addresses?["Country"] as? String else {
                    
                    completion("")
                    return
            }
            
            let address = "\(city), \(country)"
            completion(address)
            
        }
    }
    
    
    /// Get the latitude, longitude of user location, if there is no return nil
    func getUserLocation()->(latitude : Double, longitude : Double)?{
        if let currentLocation = self.currentLocation{
            let lat = currentLocation.coordinate.latitude
            let long = currentLocation.coordinate.longitude
            return (lat,long)
        }
        return nil
    }
    
    func getCurrentWeather(lat : Double, long : Double, completion : (((String?, Error?) -> Void)?) = nil) {
        
        let request = "http://api.openweathermap.org/data/2.5/weather?APPID=\(Constants.WEATHER_KEY_API)&lat=\(lat)&lon=\(long)"
        
        Alamofire.request(request, method: .get, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                guard let data = response.result.value else {
                    return
                }
                let json = JSON(data)
                let weatherArray = json["weather"]
                let weatherDescription = weatherArray.first?.1["description"].rawString()
            
                completion?(weatherDescription,nil)
                
                break
                
            case .failure(_):
              
                completion?(nil,response.result.error)
                
                break
                
            }
        }
    }

    
}

extension LocationManager : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.currentLocation = locations.last
    }
}
