//
//  File.swift
//  TheranicaBLE
//
//  Created by Maor Shams on 10/09/2017.
//  Copyright © 2017 Maor Shams. All rights reserved.
//

import Foundation

public enum TDeviceNotification{
    case connectionStatusChange
    case batteryLevelChange
    case programStateDidChange
    case bluetoothPowerDidChange
    
    public var name : String{
        switch self {
        case .connectionStatusChange  : return "deviceConnectionStatusDidChange"
        case .batteryLevelChange      : return "deviceBatteryLevelDidChange"
        case .programStateDidChange   : return "deviceProgramStateDidChange"
        case .bluetoothPowerDidChange : return "bluetoothPowerDidChange"
        }
    }
    
    public var info : String{
        switch self {
        case .connectionStatusChange  : return "deviceStatus"
        case .batteryLevelChange      : return "batteryLevel"
        case .programStateDidChange   : return "programState"
        case .bluetoothPowerDidChange : return "bluetoothPower"
        }
    }
}

