//
//  Utils.swift
//  Theranika
//
//  Created by msapps on 15/06/2017.
//  Copyright © 2017 msapps. All rights reserved.
//

import UIKit
import SCLAlertView
class Utils{
    
    class func iterateEnum<T: Hashable>(_: T.Type) -> AnyIterator<T> {
        var i = 0
        return AnyIterator {
            let next = withUnsafeBytes(of: &i) { $0.load(as: T.self) }
            if next.hashValue != i { return nil }
            i += 1
            return next
        }
        
    }
    
    class func enumCase<T>(from option : String, of enumType : T.Type ) -> T? where T: RawRepresentable , T:Hashable {
        
        let theEnum = iterateEnum(enumType)
        for item in theEnum{
            if let itemRaw = item.rawValue as? String{
                if option == itemRaw{
                    return item
                }
            }
        }
        return nil
    }
    
    class func isValidEmail(_ testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    class func isPasswordValid(_ password : String) -> Bool{
        return password.count >= 6 && !password.contains(" ")
    }
    
    class func setStatusBarColor(){
        if let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as? UIView{
            if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
                statusBar.backgroundColor = Constants.Colors.statusBar.value
            }
        }
    }
    
    class func detailsMigraineDiaryTopFormat(date : Date) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, d MMMM y"
        return formatter.string(for: date)!
    }
    
    class func detailsMigraineDiaryMiddleFormat(date : Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "(HH:mm a)"
        return formatter.string(for: date)!
    }
    
    class func migraneDiaryFormat(date : Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, MMMM d, y (HH:mm a)"
        return formatter.string(for: date)!
    }
    
    
    /// Show alert on current View controller
    /// - parameters:
    ///   - title:           The title of the alert
    ///   - message:         The message of the alert
    ///   - type:            The type of the alert
    ///   - showCloseButton: Show close button? (false by default)
    ///   - autoHide:        Automatically hide alert (TRUE by default)
    ///   - autoHideTime:    Automatically hide alert (2 sec by default)
    ///   - customButton:    Custom buttom (title : String, selector : Selector)? = nil
    ///   - completion:      Runs when finish showing(optional)
    /// - returns: instance of SCLAlertViewResponder
    class func alert(title : String, message : String = "",type : SCLAlertViewStyle,
               showCloseButton : Bool = false,
               autoHide : Bool = true,
               autoHideTime : Double = 2,
               customButton : (title : String, selector : Selector)? = nil,
               completion : (()->Void)? = nil) -> SCLAlertViewResponder{
        
        
            
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: showCloseButton
            )
            
            let alert = SCLAlertView(appearance: appearance)
            
            var alertViewResponder: SCLAlertViewResponder
            
            switch type {
            case .success: alertViewResponder = alert.showSuccess(title, subTitle: message)
            case .error:   alertViewResponder = alert.showError(title, subTitle: message)
            case .notice:  alertViewResponder = alert.showNotice(title, subTitle: message,colorStyle: 0x086899)
            case .warning: alertViewResponder = alert.showWarning(title, subTitle: message)
            case .info:    alertViewResponder = alert.showInfo(title, subTitle: message,colorStyle: 0x086899)
            case .edit:    alertViewResponder = alert.showEdit(title, subTitle: message,colorStyle: 0x086899)
            case .wait:    alertViewResponder = alert.showWait(title, subTitle: message,colorStyle: 0x086899)
            }
            
            if let customButton = customButton{
                alert.addButton(customButton.title, target: self, selector: customButton.selector)
            }
            
            if autoHide{
                DispatchQueue.main.asyncAfter(deadline: .now() + autoHideTime) {
                    alertViewResponder.close()
                }
            }
            
            if let completion = completion{
                alertViewResponder.setDismissBlock(completion)
            }
            return alertViewResponder
        }
    
}
