//
//  RoutingController.swift
//  Theranica
//
//  Created by Maor Shams on 29/08/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit

class RoutingController: NSObject {
    
    enum Storyboard : String{
        case main = "Main"
        case auth = "Authenticate"
        case onBoarding = "OnBoarding"
        case empty = "Empty"
        case lockscreen = "Lockscreen"
        case settings = "Settings"
    }
    
    static let shared = RoutingController()
    override private init() {}
    
    weak var window : UIWindow?
    
    func determineRoot(){
                
        if DBManager.manager.isLaunchedBefore{
            
            if AuthManager.manager.currentUser == nil {
                self.setStoryboard(.auth)
            }else{
                if DBManager.manager.isOnBoardingDidPresent{
                    if DBManager.manager.isEulaAproved{
                        self.setStoryboard(DBManager.manager.isEnteredAppPassword ? .main : .lockscreen)
                    }else{
                        guard let window = window else {
                            return
                        }
                        let storyboard = UIStoryboard(name:  RoutingController.Storyboard.settings.rawValue, bundle: .main)
                        let vc = storyboard.instantiateViewController(withIdentifier: "PrivacyPolicyVC")
                        vc.view.layoutIfNeeded()
                        UIView.transition(with: window,duration: 0.3,options: .transitionCrossDissolve, animations: {
                            self.window?.rootViewController = vc
                        })
                    }
                }else{
                    self.setStoryboard(.onBoarding)
                }
            }
        }else{
            AuthManager.manager.logOut(cleanUp: false, completion: { (_) in
                self.setStoryboard(.auth)
            })
        }
    }
    
    private func setStoryboard(_ storyBoard : RoutingController.Storyboard){
        guard let window = window else {
            return
        }
        let storyboard = UIStoryboard(name: storyBoard.rawValue, bundle: .main)
        let vc = storyboard.instantiateInitialViewController()
        vc?.view.layoutIfNeeded()
        UIView.transition(with: window,duration: 0.3,options: .transitionCrossDissolve, animations: {
            self.window?.rootViewController = vc
        })
    }
    
}
