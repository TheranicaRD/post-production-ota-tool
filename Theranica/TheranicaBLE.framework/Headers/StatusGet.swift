//
//  StatusGet.swift
//  Theranica
//
//  Created by Maor Shams on 21/08/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit

open class StatusGet: NSObject {
    
    public enum StatusGetError : Error{
        case invalidInitParams
        case invalidComponentsCount
    }
    
    open let programState : ProgramState
    open let programID    : String
    open let elapsedTime  : Int
    open let totalTime    : Int
    open let batteryLevel : Int
    open let amplitude    : Int
    open let statusACK    : Bool
    
    open var convertedAmplitude : Int{
        return BleUtils.convertDeviceToAppIntensity(deviceIntensity : Double(amplitude))
    }
    
    init(components : [String]) throws {
        
        guard components.count >= 6 else {
            throw StatusGetError.invalidComponentsCount
        }
        
        
        guard let state = Int(components[0]),
            let programState = ProgramState.getProgramState(from: state),
            let elapsedTime  = Int(components[2]),
            let totalTime    = Int(components[3]),
            let batteryLevel = Int(components[4]),
            let amplitude    = Int(components[5]) else{
                
                throw StatusGetError.invalidInitParams
                
        }
        
        let status       = components[6].components(separatedBy: "\r")[0]
        let statusACK    = status == "ACK" ? true : false
        let programID    = components[1]
        
        self.programState =  programState
        self.programID    =  programID
        self.elapsedTime  =  elapsedTime
        self.totalTime    =  totalTime
        self.batteryLevel =  batteryLevel
        self.amplitude    =  amplitude
        self.statusACK    =  statusACK
        
        super.init()
    }
    
    public init(programState : ProgramState,
         programID : String,
         elapsedTime : Int,
         totalTime : Int,
         batteryLevel : Int,
         amplitude : Int,
         statusACK : Bool)  {
        
        self.programState =  programState
        self.programID    =  programID
        self.elapsedTime  =  elapsedTime
        self.totalTime    =  totalTime
        self.batteryLevel =  batteryLevel
        self.amplitude    =  amplitude
        self.statusACK    =  statusACK
        
        super.init()
    }
    
    
}
