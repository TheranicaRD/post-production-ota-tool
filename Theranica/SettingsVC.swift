//
//  SettingsVC.swift
//  Theranica
//
//  Created by Maor Shams on 27/06/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit
import SCLAlertView

class SettingsVC: UIViewController {
    
    enum MenuItem : String{
        case profile = "Profile, sharing and security"
        case treatmentsAndPrograms = "Treatments and programs"
        case help = "Misc and help"
        case about = "About"
        case connect = "Connect"
        case demoMode = "Demo Mode"
        case logOut = "Log out"
        
        var segue : String{
            switch self {
            case .treatmentsAndPrograms: return "treatmentsAndProgramsSegue"
            case .profile: return "profileSharingAndSecurity"
            case .help: return "miscAndHelpScreen"
            case .about: return "aboutSegue"
            case .logOut : return ""
            default: return ""
                
            }
        }
        
        var id : String{
            switch self {
            case .treatmentsAndPrograms: return "TreatmentAndProgramsVC"
            case .profile: return "ProfileVC"
            case .help: return "MiscAndHelpVC"
            case .about: return "AboutVC"
            default: return ""
            }
        }
        
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    var menuItems = [MenuItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        BleManager.manager.startScan()
        hideBackButton()
        BleManager.manager.delegate = self
        // add menu items to array
        _ = Utils.iterateEnum(MenuItem.self).map {
            menuItems.append($0)
        }
    }
    
  
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = sender as? IndexPath{
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func configDemoMode(){
        var isDemoModeEnabled = ApplicationManager.manager.isDemoModeEnabled
        
        guard ApplicationManager.manager.currentTreatment == nil else{
            _ = Utils.alert(title: "error".localized, message : "treatment_running".localized , type: .error , autoHideTime: 2)

            return
        }
        
        // Demo mode running
        if TimerManager.manager.isDemoTreatmentTimerRunning{
            
            _ = Utils.alert(title: "error".localized, message : "demo_treatment_running".localized , type: .error , autoHideTime: 2)

            
        } // Treatment running
        else if BleManager.manager.isProgramRunning{
            
            
            _ = Utils.alert(title: "error".localized, message : "treatment_running".localized , type: .error , autoHideTime: 2)
            
            
        } // Treatment / Demo is not running
        else {
            
            if BleManager.manager.isBLEConnected{
                
                if !isDemoModeEnabled{
                    _ = Utils.alert(title: "error".localized, message : "demomode_error_1".localized , type: .error , autoHideTime: 2)
                    return
                }

            
            }
            BleManager.manager.disconnect()

            isDemoModeEnabled = !isDemoModeEnabled
            ApplicationManager.manager.setIsDemoModeEnabled(isDemoModeEnabled)
            let enabledText = isDemoModeEnabled ? "enabled".localized : "disabled".localized
            
            _ = Utils.alert(title: "demo_mode_is".localized + " \(enabledText)", type: .success , autoHideTime: 1)
        }
    
        
   
    }
    
    func showConnectAlert(){
        
        guard BleManager.manager.isBluetoothOn else{
            _ = Utils.alert(title: "error".localized, message: "bluetooth_off".localized, type: .error)
            return
        }
        
        guard !ApplicationManager.manager.isDemoModeEnabled else {
            _ = Utils.alert(title: "error".localized, message: "demomode_error_2".localized, type: .error)
            return
        }
        
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        
        let alert = SCLAlertView(appearance: appearance)
        
        
        // Add a text field
        let textField = alert.addTextField("your_device_id".localized)
        
        // if there is saved mac address show
        if let deviceID = DBManager.manager.deviceMacAddress{
            textField.text = deviceID
        }
        
        alert.addButton("connect".localized) {
            
            let loadingAnimation = Utils.alert(title: "searching".localized, message: "please_wait".localized, type: .wait, autoHide: false)
            
            guard let deviceID = textField.text?.lowercased(),
                BleManager.manager.isValidHex(deviceID) else{
                    loadingAnimation.close()
                    _ = Utils.alert(title: Constants.ERROR, message: "invalid_device_id".localized, type: .error)
                    return
            }
            
            BleManager.manager.connectToPeripheral(macAddress: deviceID, withAttempts: 10, completion: { (error) in
                DispatchQueue.main.async {
                    
                    loadingAnimation.close()
                    if let error = error{
                        _ = Utils.alert(title: Constants.ERROR, message: error.localizedDescription.localized, type: .error)
                        return
                    }
                }
            })
            
            DBManager.manager.saveDevice(macAddress: deviceID)
        
        }
        
        alert.addButton("cancel".localized, backgroundColor: .lightGray, textColor: .black, showDurationStatus: false) {}
        alert.showEdit("connect_to_device".localized, subTitle: "enter_device_id".localized,colorStyle: 0x086899)
        
    }
    
    func logOut(){
        
        let loadingAnimation = Utils.alert(title: "loading".localized, message: "please_wait".localized, type: .wait, autoHide: false)
        
        AuthManager.manager.logOut{ error in
            
            loadingAnimation.close()
            
            if let error = error{
                _ = Utils.alert(title: Constants.ERROR, message: error.localizedDescription, type: .error)
                return
            }
            
            RoutingController.shared.determineRoot()
        }
    }
    
    func handleNoSegueOptions(menuItem : MenuItem){
        
        switch menuItem {
        case .demoMode:  configDemoMode()
        case .connect :  showConnectAlert()
        case .logOut : logOut()
        default: return
            
        }
    }
    
    

    
}
extension SettingsVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CELL) as!  SettingsCell
        cell.titleLabel.text = menuItems[indexPath.row].rawValue
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuItem = menuItems[indexPath.row]
        let segueID = menuItem.segue
        
        guard !segueID.isEmpty else{
            handleNoSegueOptions(menuItem : menuItem)
            tableView.deselectRow(at: indexPath, animated: true)
            return
        }
        
        let storyBoard = UIStoryboard(name: "Settings", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: menuItem.id)
        self.navigationController?.pushViewController(vc, animated: true)
        if let idx = self.tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: idx, animated: true)
        }
       // self.performSegue(withIdentifier: segueID, sender: indexPath)
    }
}

extension SettingsVC : BleManagerDelegate{
    
    
    
   
    
    
    
    
}
