//
//  Entity.swift
//  Theranica
//
//  Created by Maor Shams on 21/02/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import Foundation
import CoreData

enum Entity : String{
    case location = "Location"
    case symptom = "Symptom"
    case treatment = "Treatment"
    
    var description : NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.rawValue, in: DBManager.manager.context)
    }
}
