//
//  Constants.swift
//  Theranika
//
//  Created by msapps on 19/06/2017.
//  Copyright © 2017 msapps. All rights reserved.
//

import UIKit

struct Constants{
    
    // API KEYS
    static let WEATHER_KEY_API = "6f1c6336a4b5a92268dc3f37225a46b8"
    static let APP_PASSWORD = "070218"
    
    static let CURRENT_USER_TOKEN = "current_user_token"
    static let CURRENT_PROVIDER_ID = "current_user_providerID"
    
    static let TREATMENT_PROGRAM_HASH = "current_treatment_hash"
    static let TREATMENT_TOTAL_TIME = "current_treatment_total_time"
    static let TREATMENT_SAVE_TIME = "current_treatment_save_time"
    static let TREATMENT_PASSED_TIME = "current_treatment_passed_time"
    static let TREATMENT_INTENSITY = "current_treatment_intensity"
    
    static let MAC_ADDRESS = "peripheral_mac_address"
    static let ON_BOARDING_PRESENTED = "isOnBoardingPresented"
    static let LAUNCHED_BEFORE = "isLaunchedBefore"
    static let EULA_APROVED = "isEulaAproved"
    static let ENTERED_APP_PASSWORD = "isEnteredAppPassword"
    static let DEMO_MODE_ENABLED = "is_demo_mode_enabled"
    
    static let CAREGIVER_NAME = "caregiver_name"
    static let CAREGIVER_ROLE = "caregiver_role"
    static let CAREGIVER_EMAIL = "caregiver_email"
    static let CAREGIVER_SHARE = "caregiver_share"
    
    static let DATE = "date"
    static let USER_ID = "userID"
    static let LATITUDE = "latitude"
    static let LONGITUDE = "longitude"
    static let LOCATION = "location"
    static let SYMPTOMS = "symptoms"
    static let WEATHER = "weather"
    static let ADDRESS = "address"
    
    static let CELL = "cell"
    
    static let HEADACHE_SIDE = "headacheSide"
    static let NAME = "name"
    static let PAIN_LEVEL = "painLevel"
    
    static let MY_TREATMENT_PROGRAM = "my_treatment_program"
    static let MY_TREATMENT_PROGRAM_TIME = "my_treatment_program_time"
    
    // SEGUES
    static let SYMPTOMS_SEGUE = "symptomsSegue"
  


    static let ERROR = "Error"

    enum Colors{
        case statusBar
        case menuCell
        case selectedMenu
        case borderLiteGray
        case settingsSelectedTreatmentCell
        
        var value : UIColor{
            switch self {
            case .statusBar                     : return UIColor(red: 138/255, green: 195/255, blue: 65/255, alpha: 1)
            case .menuCell                      : return UIColor(red: 131/255, green: 179/255, blue: 204/255, alpha: 1)
            case .selectedMenu                  : return UIColor(red: 8/255, green: 104/255, blue: 153/255, alpha: 1)
            case .borderLiteGray                : return UIColor(red: 200/255, green: 199/255, blue: 204/255, alpha: 1)
            case .settingsSelectedTreatmentCell : return UIColor(red: 151/255, green: 202/255, blue: 91/255, alpha: 1)
            }
        }
    }
}
