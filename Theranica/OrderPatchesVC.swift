//
//  orderPatchesVC.swift
//  Theranica
//
//  Created by Maor Shams on 27/06/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit

class OrderPatchesVC: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func walmartAction(_ sender: UIButton) {
        let url = URL(string: "https://www.walmart.com")!
        open(url: url)
    }
    
    @IBAction func amazonAction(_ sender: UIButton) {
        let url = URL(string: "https://www.amazon.com")!
        open(url: url)
    }
    
    
    func open(url : URL){
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    
    
}


