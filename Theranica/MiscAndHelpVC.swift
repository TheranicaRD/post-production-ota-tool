//
//  MiscAndHelpVC.swift
//  Theranica
//
//  Created by Maor Shams on 07/09/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit

class MiscAndHelpVC: UIViewController {

    @IBOutlet weak var appVersionLabel: UILabel!
    @IBOutlet weak var lastUpdatedLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()


        let appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let appBundle = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String

        appVersionLabel.text = appBundle//appVersion
        
        
    }



}
