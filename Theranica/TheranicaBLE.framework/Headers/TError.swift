//
//  TError.swift
//  TheranicaBLE
//
//  Created by Maor Shams on 10/09/2017.
//  Copyright © 2017 Maor Shams. All rights reserved.
//

import Foundation

public enum TError : Error{
    
    case deviceNotFound
    case bluetoothOff
    
    public var localizedDescription : String{
        switch self {
        case .deviceNotFound : return "no_device_title"
        case .bluetoothOff   : return "Bluetooth is off"
        }
    }
}
