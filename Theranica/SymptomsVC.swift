//
//  SymptomsVC.swift
//  Theranika
//
//  Created by msapps on 21/06/2017.
//  Copyright © 2017 msapps. All rights reserved.
//

import UIKit
import SwiftyJSON
protocol SymptomDelegate {
    func userDidFinishAnswer(symptoms : [Symptom])
}

enum SymptomQuestion{
    
    case headache_level
    case headache_side
    case nausea
    case aura
    case photophobia
    case phonophobia
    
    var title : String{
        switch self {
        case .aura: return "AURA"
        case .headache_level: return "HEADACHE LEVEL"
        case .headache_side: return "HEADACHE SIDE"
        case .nausea: return "NAUSEA"
        case .photophobia : return "PHOTOPHOBIA"
        case .phonophobia : return "PHONOPHOBIA"
        }
    }
    
    var symotomType : Any{
        switch self {
        case .aura:           return Symptom.PainLevel.none
        case .headache_level: return Symptom.PainLevel.none
        case .headache_side:  return Symptom.HeadacheSide.none
        case .nausea:         return Symptom.PainLevel.none
        case .photophobia :   return Symptom.PainLevel.none
        case .phonophobia :   return Symptom.PainLevel.none
        }
    }
    
    var key : String{
        switch self {
        case .aura:           return "aura"
        case .headache_level: return "headacheLevel"
        case .headache_side:  return "headacheSide"
        case .nausea:         return "nausea"
        case .photophobia :   return "photophobia"
        case .phonophobia :   return "phonophobia"
        }
    }
}

class SymptomsVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var topView: UIView!
    
    let symptomPainOptions = Symptom.PainLevel.values
    let symptomHeadacheSides = Symptom.HeadacheSide.values
    
    // the tag will be used for getting the selected button in each cell
    
    var symptomQuestions : [(symptomQuestion : SymptomQuestion, selectedButtonTag : Int?)] = [
        (SymptomQuestion.headache_level,nil),
        (SymptomQuestion.headache_side,nil),
        (SymptomQuestion.nausea,nil),
        (SymptomQuestion.aura,nil),
        (SymptomQuestion.photophobia,nil),
        (SymptomQuestion.phonophobia,nil)
    ]
    
    
    var delegate : SymptomDelegate?
    var selectedSymtoms : [Symptom] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageControl.numberOfPages = symptomQuestions.count
        LocationManager.manager.fetchUserLocation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        
        if (self.isMovingFromParentViewController){
            
            // Save diary in DB
            NetworkManager.manager.sendNewDiaryToDB(symptoms: self.selectedSymtoms ){
                
                // if there is an error, save locally
                if let error = $0{
                    print(error.localizedDescription)
                    DBManager.manager.saveTreatmentInCoreData(symptoms: self.selectedSymtoms)
                }else{
                    DBManager.manager.clearContext()
                }
            }
            
            self.delegate?.userDidFinishAnswer(symptoms: self.selectedSymtoms)
            LocationManager.manager.stopFetchingUserLocation()
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.collectionView {
            var currentCellOffset = self.collectionView.contentOffset
            currentCellOffset.x += self.collectionView.frame.width / 2
            if let indexPath = self.collectionView.indexPathForItem(at: currentCellOffset) {
                self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.collectionView {
            let pageWidth = scrollView.frame.width
            self.pageControl.currentPage = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
        }
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        userDidFinishAnswer()
    }
    
    @IBAction func discardAction(_ sender: UIButton) {
        popViewController()
    }
    
    func scrollToNextQuestion(){
        guard let indexPath = collectionView.indexPathsForVisibleItems.first else{
            return
        }
        
        if indexPath.item + 1 < symptomQuestions.count {
            collectionView.scrollToItem(at: IndexPath(item: indexPath.item + 1 , section: indexPath.section), at: .left, animated: true)
        }else{
            // user has arrived to final question, check if he did'nt skip one of them
            if selectedSymtoms.count == symptomQuestions.count{
                self.userDidFinishAnswer()
            }
        }
    }
    
    
    func userDidFinishAnswer(){
        popViewController()
    }
    
    func popViewController(){
        self.navigationController?.popViewController(animated: true)
    }
    
}

// MARK: - UICollectionView
extension SymptomsVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return symptomQuestions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CELL, for: indexPath) as! SymptomCell
        
        
        let currentSymtom = symptomQuestions[indexPath.item]
        let options = currentSymtom.symptomQuestion.symotomType is Symptom.PainLevel ? symptomPainOptions : symptomHeadacheSides
        let tag = currentSymtom.selectedButtonTag
        let isHeadacheLevel = currentSymtom.symptomQuestion == SymptomQuestion.headache_level
        cell.configCell(with: currentSymtom.symptomQuestion.title, options: options, selectedButtonTag:  tag, isHeadacheLevel: isHeadacheLevel)
        cell.delegate = self
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
}
// MARK: - SymptomCellDelegate
extension SymptomsVC : SymptomCellDelegate{
    
    func symptomDidSelect(symptom: Symptom, selectedButtonTag: Int) {
        
        guard let indexPath = collectionView.indexPathsForVisibleItems.first else{
            return
        }
        
        symptomQuestions[indexPath.row].selectedButtonTag = selectedButtonTag
        
        selectedSymtoms.append(symptom)
        
        self.scrollToNextQuestion()
    }
    
    func symptomDidDeselect(symptom: Symptom, selectedButtonTag: Int) {
        guard let indexPath = collectionView.indexPathsForVisibleItems.first else{
            return
        }
        
        symptomQuestions[indexPath.row].selectedButtonTag = nil
        
        for i in 0..<selectedSymtoms.count{
            
            if selectedSymtoms[i].name == symptom.name{
                selectedSymtoms.remove(at: i)
                return
            }
        }
    }
    
}
