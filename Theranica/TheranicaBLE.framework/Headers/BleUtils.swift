//
//  BleUtils.swift
//  Theranica
//
//  Created by Maor Shams on 21/08/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import Foundation

open class BleUtils{
    
   open class func isValidHex(_ deviceID : String) -> Bool{
        guard !deviceID.isEmpty else{
            return false
        }
        let id = deviceID.replacingOccurrences(of: ":", with: "")
        
        guard id.isHex else{
            return false
        }
        
        return true
    }
    
    open class func convertAppToDeviceIntensity(from level : Double) -> Double{
        let MAX = 67.0
        let MIN = 8.0
        let _level = Double(level) / Double(100.0)
        return  (_level * Double((MAX-MIN)) + MIN)
    }
    
    
    open class func convertDeviceToAppIntensity(deviceIntensity : Double) -> Int{
        let MAX_INTENSITY_SCALE = 100.0
        let minProgramIntensityLevel = 8.0
        let maxProgramIntensityLevel = 67.0
        let tmp = (MAX_INTENSITY_SCALE * (deviceIntensity - minProgramIntensityLevel)) / (maxProgramIntensityLevel - minProgramIntensityLevel)
        return Int(tmp.rounded())
    }
    
    
    open class func normalizeAmptitude(deviceAmp : Int, userAmp : Int) -> Int{
        return (deviceAmp + 1 == userAmp || deviceAmp - 1 == userAmp) ? userAmp : deviceAmp
    }
    
}

extension String{
    
    public var peripheralName : String?{
        if let str = self.split(separator: "-").last {
            return String(Array(str))
        }
        return nil
    }
    
    public func utfToHex() -> Data?{
        return self.data(using: .utf8)
    }
    
    public var isHex : Bool{
        return self.utfToHex() != nil
    }
    
    public func hexToHex() -> Data {
        var hex = self
        var data = Data()
        while(hex.count > 0) {
            let c: String = hex.substring(to: hex.index(hex.startIndex, offsetBy: 2))
            hex = hex.substring(from: hex.index(hex.startIndex, offsetBy: 2))
            var ch: UInt32 = 0
            Scanner(string: c).scanHexInt32(&ch)
            var char = UInt8(ch)
            data.append(&char, count: 1)
        }
        return data
    }
}

extension Data {
    public func hexToStringHex() -> String {
        return map { String(format: "%02x", $0) }
            .joined(separator: "")
    }
    
    public func hexToUtf() -> String?{
        return String(data: self, encoding: .utf8)
    }
    
    public var bytes : [UInt8]{
        let this = [UInt8](self)
        return this.filter { $0 <= 127 }
    }
}
extension Array{
    public var data : Data?{
        return (self is Array<UInt8>) ? Data(bytes:(self as! Array<UInt8>)) : nil
    }
}



