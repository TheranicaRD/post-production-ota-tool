
//
//  DBManager.swift
//  OldCoreDataSample
//
//  Created by Maor Shams on 04/07/2017.
//  Copyright © 2017 msapps. All rights reserved.
//

import UIKit
import CoreData

class DBManager: NSObject {
    
    static let manager = DBManager()
    override private init(){}

    /// Save device mac address in User Defaults
    func saveDevice(macAddress : String){
        UserDefaults.standard.set(macAddress, forKey: Constants.MAC_ADDRESS)
    }
    
    /// Get device mac address from User Defaults
    var deviceMacAddress : String?{
        return UserDefaults.standard.string(forKey: Constants.MAC_ADDRESS)
    }

    /// Remove all saved data from User defaults
    func clearUserDefaults(){
        let standard = UserDefaults.standard
        standard.dictionaryRepresentation().forEach {
            if $0.key != Constants.EULA_APROVED && $0.key != Constants.ENTERED_APP_PASSWORD{
                standard.removeObject(forKey: $0.key)
            }
        }
    }

    /// Check if On-Boarding Did Present
    var isOnBoardingDidPresent : Bool{
        return UserDefaults.standard.bool(forKey: Constants.ON_BOARDING_PRESENTED)
    }
    
    /// Save On-Boarding Did Present
    func saveDidPresentOnBoarding(){
        UserDefaults.standard.set(true, forKey: Constants.ON_BOARDING_PRESENTED)
    }
    
    /// Check if is Launched Before & save as true
    var isLaunchedBefore : Bool{
        let launchedBefore = UserDefaults.standard.bool(forKey: Constants.LAUNCHED_BEFORE)
        UserDefaults.standard.set(true, forKey: Constants.LAUNCHED_BEFORE)
        return launchedBefore
    }
    
    /// Check if is Launched Before & save as true
    var isEulaAproved : Bool{
        let eulaAproved = UserDefaults.standard.bool(forKey: Constants.EULA_APROVED)
        
        return eulaAproved
    }
    
    /// Check if App Password is saved
    var isEnteredAppPassword : Bool{
        return  UserDefaults.standard.bool(forKey: Constants.ENTERED_APP_PASSWORD)
    }
    
    /// Save App Password did Answer
    func saveIsEnteredAppPassword(){
        UserDefaults.standard.set(true, forKey: Constants.ENTERED_APP_PASSWORD)
    }

    /// Save
    func saveTreatmentProgram(counter : Int,countdown : Int, program : TreatmentProgram){
        // current program hash
        UserDefaults.standard.set(program.type.hashValue , forKey: Constants.TREATMENT_PROGRAM_HASH)
        // current program total time
        UserDefaults.standard.set(program.totalTime , forKey: Constants.TREATMENT_TOTAL_TIME)
        // closing app save time
        UserDefaults.standard.set(Date() , forKey: Constants.TREATMENT_SAVE_TIME)
        // the time passed from the beggining of the program
        let timePassed = counter - countdown
        UserDefaults.standard.set(timePassed , forKey: Constants.TREATMENT_PASSED_TIME)
    }
    
    func removeSavedProgram(){
        UserDefaults.standard.set(nil , forKey: Constants.TREATMENT_PROGRAM_HASH)
        UserDefaults.standard.set(nil , forKey: Constants.TREATMENT_TOTAL_TIME)
        UserDefaults.standard.set(nil , forKey: Constants.TREATMENT_SAVE_TIME)
        UserDefaults.standard.set(nil , forKey:  Constants.TREATMENT_PASSED_TIME)
    }
    
    func saveIsEulaAproved(isAproved : Bool){
        UserDefaults.standard.set(isAproved, forKey: Constants.EULA_APROVED)
    }
    
    /// Create Symptom
    func generateSymptom( symptomName : String,
                          painLevel : Symptom.PainLevel? = nil,
                          headacheSide : Symptom.HeadacheSide? = nil) -> Symptom?{
        
        if let symptomDescription = Entity.symptom.description{
            let symptom = Symptom(entity: symptomDescription, insertInto: context)
            symptom.setValue(symptomName, forKey: Constants.NAME)
            symptom.setValue(painLevel?.rawValue, forKey: Constants.PAIN_LEVEL)
            symptom.setValue(headacheSide?.rawValue, forKey: Constants.HEADACHE_SIDE)
            return symptom
        }
        
        return nil
    }
    
    /// Create Symptom
    func generateSymptom( symptomName : String, buttonTitle : String) -> Symptom?{
        
        // Check if the button is PainLevel
        if let painLevel = Symptom.PainLevel.enumCase(from: buttonTitle) {
            let s = generateSymptom(symptomName: symptomName, painLevel: painLevel)
            
            return s
        }//  Check if the button is HeadacheSide
        else if let headacheSide = Symptom.HeadacheSide.enumCase(from: buttonTitle){
            return generateSymptom(symptomName: symptomName, headacheSide: headacheSide)
        }
        
        return nil
    }
    
    /// Update Symptom in Core Data
    func updateSymptomInCoreData(_ symptom : Symptom, completion : (((Error?) -> Void)?) = nil){
        do{
            try symptom.managedObjectContext?.save()
            completion?(nil)
        }catch{
            completion?(error)
        }
    }
    
    /// Save Treatment in Core Data
    func saveTreatmentInCoreData(symptoms : [Symptom]){
        
        guard let treatmentDescription = Entity.treatment.description,
            let locationDescription  = Entity.location.description ,
            let currentUserID = AuthManager.manager.currentUser?.uid else{
                return
        }
        
        // create new treatment Object
        
        let treatment = Treatment(entity: treatmentDescription, insertInto: context)
        treatment.setValue(Date(), forKey: Constants.DATE)
        treatment.setValue(currentUserID, forKey: Constants.USER_ID)
        
        // get current user address
        
        if let location = LocationManager.manager.getUserLocation(){
            
            // create new location
            let coreDataLocation = Location(entity: locationDescription, insertInto: context)
            
            // save properties
            coreDataLocation.setValue(location.latitude, forKey: Constants.LATITUDE)
            coreDataLocation.setValue(location.longitude, forKey: Constants.LONGITUDE)
            
            addStringAddress(to: treatment)
            
            // add location to treatment
            treatment.setValue(coreDataLocation, forKey: Constants.LOCATION)
            
            // get current user weather
            addCurrentWeather(to: coreDataLocation, lat: location.latitude, long: location.longitude)
            
        }
        
        let set = NSSet(array: symptoms)
        treatment.setValue(set, forKey: Constants.SYMPTOMS)
        do{
            try treatment.managedObjectContext?.save()
        }catch{
            debugPrint(error.localizedDescription)
        }
        
    }
    
    /// Add current weather to location
    private func addCurrentWeather(to location : Location, lat : Double, long : Double){
        
        LocationManager.manager.getCurrentWeather(lat: lat, long: long, completion: { (weather, error) in
            
            location.setValue(weather, forKey: Constants.WEATHER)
            
            do {
                try location.managedObjectContext?.save()
            }catch{
                debugPrint(error.localizedDescription)
            }
        })
        
    }
    
    /// Add Address to treatment
    private func addStringAddress(to treatment : Treatment){
        if let location = treatment.location {
            LocationManager.manager.getUserAddress(from: location) { (address) in
                location.setValue(address, forKey: Constants.ADDRESS)
                try? treatment.managedObjectContext?.save()
            }
        }
    }
    
    /// Fetch Core Data Treatments
    func fetchCoreDataTreatments(with completion : ([Treatment]?)->Void){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Entity.treatment.rawValue)
        do {
            if let result = try self.managedObjectContext.fetch(fetchRequest) as? [Treatment]{
                completion(result)
            }
        } catch {
            let fetchError = error as NSError
            completion(nil)
            debugPrint(fetchError)
        }
        
    }
    
    /// Delete diary from core data
    func deleteDiaryFromCoreData(_ treatment : Treatment){
        if let location = treatment.location{
            treatment.managedObjectContext?.delete(location)
        }
        if let symptoms = treatment.symptoms {
            for obj in symptoms{
                treatment.managedObjectContext?.delete(obj as! NSManagedObject)
            }
        }
        treatment.managedObjectContext?.delete(treatment)
        do {
            try treatment.managedObjectContext?.save()
        }catch{
            debugPrint("core data error \(error.localizedDescription)")
        }
        
    }
    
    // MARK: - Core Data stack

    var context : NSManagedObjectContext{
        return managedObjectContext
    }
    
    func clearContext(){
        context.reset()
    }
    
    func isOldVersionOfCoreData() -> Bool{
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
            return false
        } catch {
            return true
        }
    }
    
    private lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.tutsplus.Core_Data" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        // **CHANGE THE NAME OF THE RESOURCE**
        let modelURL = Bundle.main.url(forResource: "Model", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    private lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    private lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
}



