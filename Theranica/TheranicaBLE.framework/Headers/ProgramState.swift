//
//  ProgramState.swift
//  Theranica
//
//  Created by Maor Shams on 21/08/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import Foundation

@objc public enum ProgramState: Int{
    
    case notLoaded = 1
    case ready     = 2
    case running   = 3
    case paused    = 4
    case stopped   = 5
    case ended     = 6
    
    public static func getProgramState(from value : Int) -> ProgramState?{
        switch value {
        case 1: return .notLoaded
        case 2: return .ready
        case 3: return .running
        case 4: return .paused
        case 5: return .stopped
        case 6: return .ended
        default: return nil
        }
    }
    
}
