//
//  BLConnectionState.swift
//  Theranica
//
//  Created by Maor Shams on 21/08/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import Foundation

@objc public enum BLConnectionState: Int{
    case connected = 1
    case disconnected = 0
    public var value : String{
        switch self {
        case .disconnected : return "disconnected"
        case .connected : return "connected"
        }
    }
}

