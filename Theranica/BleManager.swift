//
//  BleManager.swift
//  Theranica
//
//  Created by Maor Shams on 10/09/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit
import CoreBluetooth
import TheranicaBLE
import SwiftyJSON
import FCUUID

@objc protocol BleManagerDelegate {
    @objc optional func deviceConnectionStatusDidChange(to state : BLConnectionState)
    @objc optional func deviceBatteryLevelDidChange(to level : Int)
    @objc optional func deviceDidRecivedStatusGet(_ statusGet : StatusGet)
    @objc optional func deviceDidStartTreatment(statusGet : StatusGet, treatmentProgram : TreatmentProgram?)
    @objc optional func deviceAmplitudeDidChange(to level : Int)
    @objc optional func deviceDidStoppedTreatment()
    @objc optional func deviceDidFinishTreatment()
    @objc optional func deviceDidRecivedVersion(version: String)
    @objc optional func bluetoothPowerDidChange(isOn: Bool)

}

class BleManager : NSObject{
    
    // Init
    static let manager = BleManager()
    let tManager = TDeviceManager.manager
    
    override private init(){
        super.init()
        tManager.initLibrary(with: self,
                             mode: .production,
                             uniqueIdentifier: FCUUID.uuidForDevice())
    }
    
    var delegate : BleManagerDelegate?
    
    func startScan(){
        tManager.startScan()
    }
    
    func stopScan(){
        tManager.stopScan()
    }
    func disconnect(){
        tManager.disconnect()
    }
    var lastStatusGet : StatusGet?{
        return tManager.getLastStatusGet()
    }
    
    let debouncedFunction = debounce(interval: 1, queue: .main) { (level : Double) in
        TDeviceManager.manager.setIntensity(levelInPercentage: level)
    }
    
    func setIntensity(levelInPercentage : Int){
       debouncedFunction(Double(levelInPercentage))
    }
    
    var isBLEConnected : Bool{
        return tManager.isBLEConnected
    }
    
    var isProgramRunning : Bool{
        if self.isBLEConnected, let lastStatusGet = self.lastStatusGet, lastStatusGet.programState == .running{
            return true
        }
        return false
    }
    
    var isBluetoothOn : Bool{
        return tManager.isBluetoothOn
    }
    
    func stopTreatment(){
        tManager.stopTreatment()
    }
    
    func connectToPeripheral(macAddress : String,withAttempts : Int = 1 ,completion  : ((TError?) -> Void)? = nil){
        
        tManager.connectToPeripheral(macAddress: macAddress, withAttempts: withAttempts, completion: { error in
            completion?(error)
        })
    }
    
    /// Check if device is not connected,
    /// if there is saved program and bluetooth is on.
    /// Then connect
    func connectToDeviceIfNedded(){
        if !isProgramRunning,
            !isBLEConnected,
            isBluetoothOn,
           ApplicationManager.manager.currentTreatment != nil,
            let macAddress = DBManager.manager.deviceMacAddress{
            connectToPeripheral(macAddress: macAddress, withAttempts: 5)
        }
    }
    
    func isValidHex(_ deviceID : String) -> Bool{
        return BleUtils.isValidHex(deviceID)
    }
    
    func startTreatmentProgram(programNum : Int , with intensityPercentage : Int){
        
        guard let file = Bundle.main.url(forResource: "programs", withExtension: "json"),
            let data = try? Data(contentsOf: file) else{
                return
        }
        
        let json = JSON(data: data)
        let jsonPrograms = json["program"]
        let jsonCycles = json["cycle"]
        let jsonPulses = json["pulse"]
        
        let program = jsonPrograms[programNum]
        let programCycles = program["cycles"]
        let programInfo = program["info"]
        
        var pulsesToSend = [String]()
        var cyclesToSend = [String]()
        var programToSend = ""
        
        func getPules(){
            
            for i in 0..<programCycles.count{
                
                let programCycle = programCycles[i]
                
                
                guard let programCycleIndex = programCycle["cycleIndex"].int else{ // cycleID
                    print("programCycleIndex ERROR")
                    return
                }
                
                let jsonCycle = jsonCycles[programCycleIndex][0]
                
                guard let pulseIndex = jsonCycle["pulseIndex"].int else{ // pulse ID
                    print("pulseIndex ERROR")
                    return
                }
                
                let pulse = jsonPulses[pulseIndex]
                
                
                guard let startSendingIndex = programCycles[0]["cycleIndex"].int,
                    let pulseWave = pulse["posWave"].string,
                    let posPulseType = PulseType.getPulseType(from: pulseWave)?.rawValue,
                    let posAmplitude = pulse["posAmp"].int,
                    let posPulseTime = pulse["posTime"].int,
                    let posNegRechargeTime = pulse["posNegDelay"].int,
                    let _negPulseType = pulse["negWave"].string,
                    let negPulseType = PulseType.getPulseType(from: _negPulseType)?.rawValue,
                    let negAmplitude = pulse["negAmp"].int,
                    let negPulseTime = pulse["negTime"].int,
                    let EndRechargeTime = pulse["endDelay"].int else{
                        return
                }
                
                let pulseID = (pulseIndex - startSendingIndex == 0) ? 1 : (pulseIndex - (startSendingIndex - 1))
                
                
                let pulseToSend = "PULSE \(pulseID) \(posPulseType) \(posAmplitude) \(posPulseTime) \(posNegRechargeTime) \(negPulseType) \(negAmplitude) \(negPulseTime) \(EndRechargeTime)\r\n"
                
                
                var isPulseExist = false
                for p in pulsesToSend{
                    let components = p.components(separatedBy: " ")
                    if components[1] == String(pulseID){
                        isPulseExist = true
                        break
                    }
                }
                
                if !isPulseExist { pulsesToSend.append(pulseToSend) }
                
                
                guard let programID   = programInfo["reportId"].int,
                    let amp           = jsonCycle["pulseAmp"].int,
                    let delay         = jsonCycle["endDelay"].int,
                    let _repeat       = jsonCycle["numOfRepetitions"].int else{
                        return
                }
                
                let numOfCycles   = programCycles.count
                let numOfPulses = jsonCycles[programCycleIndex].count
                let cycleID = (programCycleIndex - startSendingIndex == 0) ? 1 : (programCycleIndex - (startSendingIndex - 1))
                let cycleToSend = "CYCLE \(cycleID) \(numOfPulses) \(pulseID) \(amp) \(delay) \(_repeat)\r\n"
                
                var isCycleExist = false
                for c in cyclesToSend{
                    let components = c.components(separatedBy: " ")
                    if components[1] == String(cycleID){
                        isCycleExist = true
                        break
                    }
                }
                
                if !isCycleExist {
                    cyclesToSend.append(cycleToSend)
                    if programToSend.isEmpty { programToSend.append("PROGRAM \(programID) \(numOfCycles) ") }
                    programToSend.append("\(cycleID) \(amp) \(delay) \(_repeat)")
                }
                
            }
            programToSend.append("\r")
        }
        
        getPules()
        
        if  let data = "VERSION\r".utfToHex(){
            tManager.writeToPeripheral(data: data)
        }
        
        if  let data = "CLEAN\r".utfToHex(){
            tManager.writeToPeripheral(data: data)
        }
        
        for pulse in pulsesToSend{
            if let data = pulse.utfToHex(){
                tManager.writeToPeripheral(data: data)
            }
        }
        for cycle in cyclesToSend{
            if let data = cycle.utfToHex(){
                tManager.writeToPeripheral(data: data)
            }
        }
        
        if  let data = programToSend.utfToHex(){
            tManager.writeToPeripheral(data: data)
        }
       
        let intensity = BleUtils.convertAppToDeviceIntensity(from: Double(intensityPercentage))
        if  let data = "AMPLITUDE \(intensity)\r".utfToHex(){
            tManager.writeToPeripheral(data: data)
        }
        
        if  let data = "STATUS START\r".utfToHex(){
            tManager.writeToPeripheral(data: data)
        }
        
        if  let data = "STATUS GET\r".utfToHex(){
            tManager.writeToPeripheral(data: data)
        }
    }
    
    func getStatus(){
        tManager.getStatus()
    }
}
extension BleManager : TDeviceManagerDelegate{
    
    func bluetoothPowerDidChange(isOn: Bool) {
        self.delegate?.bluetoothPowerDidChange?(isOn: isOn)
        if isOn, !ApplicationManager.manager.isDemoModeEnabled{
            if let uid = BleManager.manager.tManager.uuid{
                BleManager.manager.tManager.connectToPeripheral(uuid: uid, withAttempts: 3)
            }else{
                BleManager.manager.connectToDeviceIfNedded()
            }
        }
        
    }
    
    
    
    func tDeviceDidRecivedVersion(version: String) {
        self.delegate?.deviceDidRecivedVersion?(version: version)
    }
    
    func tDeviceBatteryLevelDidChange(to level: Int, statusGet: StatusGet) {
        self.delegate?.deviceBatteryLevelDidChange?(to: level)
    }
    
    func tDeviceAmplitudeDidChange(to level: Int, statusGet: StatusGet) {
        self.delegate?.deviceAmplitudeDidChange?(to: level)
    }
    
    func tDeviceDidStoppedTreatment(statusGet: StatusGet?) {
        self.delegate?.deviceDidStoppedTreatment?()
    }
    
    func tDeviceDidFinishedTreatment(statusGet: StatusGet) {
        self.delegate?.deviceDidFinishTreatment?()
    }
    
    func tDeviceDidStartTreatment(_ statusGet: StatusGet) {
        self.delegate?.deviceDidStartTreatment?(statusGet: statusGet, treatmentProgram: nil)
    }
    
    func peripheralDidDiscover(peripheral: CBPeripheral){
    }
    
    func tDeviceConnectionStatusDidChange(to state : BLConnectionState){
        self.delegate?.deviceConnectionStatusDidChange?(to: state)
        if state == .connected{
            TimerManager.manager.startStatusGetTimer()
            stopScan()

        }else{
            TimerManager.manager.stopStatusGetTimer()
            if let uuid = tManager.uuid,!ApplicationManager.manager.isDemoModeEnabled{
                tManager.connectToPeripheral(uuid: uuid)
            }
        } 
    }
    
    func tDeviceDidRecivedStatusGet(_ statusGet : StatusGet){
        self.delegate?.deviceDidRecivedStatusGet?(statusGet)
        
        
        /// At the first status get
        if  BleManager.manager.lastStatusGet == nil,
            let treatment = ApplicationManager.manager.currentTreatment{
            
            /// Check if program is not running,
            /// but we still have saved program locally
            /// This can be happend if the user removed app from backround in
            /// the middle of a treatment and stopped the device
            if statusGet.programState != .running{
                ApplicationManager.manager.clearCurrentTreatment()
            }else{
                self.delegate?.deviceDidStartTreatment?(statusGet: statusGet, treatmentProgram: treatment)
            }
        }
    }
    

}

