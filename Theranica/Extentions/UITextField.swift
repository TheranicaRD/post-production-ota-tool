//
//  UITextField.swift
//  Theranica
//
//  Created by Maor Shams on 21/02/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit
extension UITextField{
    func addToolBar(with items : [UIBarButtonItem]? = nil){
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        toolBar.items = items
        self.inputAccessoryView = toolBar
    }
}
