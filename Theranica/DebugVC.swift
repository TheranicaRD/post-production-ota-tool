//
//  DebugVC.swift
//  Theranica
//
//  Created by Maor Shams on 14/02/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit

class DebugVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    @IBAction func fakeConnectionDidChange(_ sender: UIButton) {
        BleManager.manager.tDeviceConnectionStatusDidChange(to: sender.tag == 1 ? .connected : .disconnected)
    }
    
    
}
