//
//  AboutViewController.swift
//  Theranica
//
//  Created by MSApps on 13/06/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    
    @IBOutlet weak var vesrionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            self.vesrionLabel.text = version
        }
        
        // Do any additional setup after loading the view.
    }

}
