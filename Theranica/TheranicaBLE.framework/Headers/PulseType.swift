//
//  PulseType.swift
//  Theranica
//
//  Created by Maor Shams on 21/08/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import Foundation
@objc public enum PulseType: Int{
    
    case square = 1
    case triangle = 2
    case saw = 3
    case sinus = 4
    
    public static func getPulseType(from posWave : String) -> PulseType?{
        switch posWave {
        case "Square"   : return .square
        case "Triangle" : return .triangle
        case "Saw"      : return .saw
        case "Sinus"    : return .sinus
        default: return nil
        }
    }
    
}
