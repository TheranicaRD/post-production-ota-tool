//
//  TreatmentVC.swift
//  Theranika
//
//  Created by Maor Shams on 16/06/2017.
//  Copyright © 2017 msapps. All rights reserved.
//

import UIKit
import SCLAlertView
import TheranicaBLE
class TreatmentVC: UIViewController {
    
    @IBOutlet weak var intensityPercentage: UILabel!
    @IBOutlet weak var timerStartLabel: UILabel!
    @IBOutlet weak var timerRemainingLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var intensitySlider: UISlider!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    
    var programsArray : [TreatmentProgram.ProgramType] = []
    
    var isProgramSelected = false
    var currentAmp : Int?
    var loader: SCLAlertViewResponder?
    
    // MARK: - Override methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        changePlusMinusButtonTo(isEnable: false)
        
        //  BleManager.manager.connectToDeviceIfNedded()
        
        TimerManager.manager.delegate = self
        BleManager.manager.delegate = self
        
        progressBar.setProgress(0, animated: true)
        
        // get treatmentPrograms from enum
        programsArray = Utils.iterateEnum(TreatmentProgram.ProgramType.self).map{ $0 }
        
        
        self.loader = Utils.alert(title: "loading".localized,
                                  message: "please_wait".localized,
                                  type: .wait,
                                  autoHide: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if BleManager.manager.lastStatusGet?.programState == .running{
            let intensity = getUserIntensity()
            intensityPercentage.text = "\(intensity)%"
            intensitySlider.value = Float(intensity)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Collectionview done loading
        
        if ApplicationManager.manager.isDemoModeEnabled{
            
            if let program = TimerManager.manager.getTreatmentProgram{
                setupExistProgram(with: program)
            }
        }else{
            if BleManager.manager.isProgramRunning,
                let program = ApplicationManager.manager.currentTreatment{
                
                setupExistProgram(with: program)
            }
        }
        
        self.loader?.close()
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        currentAmp = nil
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.SYMPTOMS_SEGUE, let nextVC = segue.destination as? SymptomsVC{
            nextVC.delegate = self
        }
    }
    
    func changePlusMinusButtonTo(isEnable : Bool){
        plusButton.isEnabled = isEnable
        minusButton.isEnabled = isEnable
    }
    
    // MARK: - IBAction's
    
    @IBAction func minusButtonAction(_ sender: UIButton) {
        
        var level = Int(intensitySlider.value)
        
        guard level > 0 else{
            return
        }
        
        level -= 1
        
        intensityPercentage.text = "\(level)%"
        intensitySlider.value -= 1
        
        
        saveUserIntensity(percentage: level)
        setIntensity(levelInPercentage: level)
        
    }
    
    @IBAction func plusButtonAction(_ sender: UIButton) {
        
        var level = Int(intensitySlider.value)
        
        guard level < 100 else{
            return
        }
        
        level += 1
        
        intensityPercentage.text = "\(level)%"
        intensitySlider.value += 1
        
        saveUserIntensity(percentage: level)
        setIntensity(levelInPercentage: level)
    }

    // MARK: - UICollectionView cells handling
    
    /// Select cell
    /// - parameters:
    ///   - indexPath: indexPath of the cell
    func selectCell(at indexPath : IndexPath){
        UIView.animate(withDuration: 0.3) {
            if let cell = self.collectionView.cellForItem(at: indexPath) as? TreatmentCell {
                cell.isPressed = true
                self.cellsInteractionEnabled(isEnabled: false, exceptCell: cell)
            }
        }
    }
    
    /// Deselect cell
    /// - parameters:
    ///   - indexPath: indexPath of the cell
    func deselectCell(at indexPath : IndexPath){
        UIView.animate(withDuration: 0.3) {
            if let cell = self.collectionView.cellForItem(at: indexPath) as? TreatmentCell{
                cell.isPressed = false
                self.cellsInteractionEnabled(isEnabled: true, exceptCell: cell)
            }
        }
    }
    
    func deselectCell(_ cell : TreatmentCell){
        UIView.animate(withDuration: 0.3) {
            cell.isPressed = false
            self.cellsInteractionEnabled(isEnabled: true, exceptCell: cell)
        }
    }
    
    /// Disable / Enable the user interaction of all cells, except one cell (clicked)
    /// - parameters:
    ///   - isEnabled: Disable / Enable the interaction
    ///   - exceptCell: the cell to skip on
    func cellsInteractionEnabled(isEnabled : Bool , exceptCell : TreatmentCell){
        collectionView.visibleCells.flatMap { $0 as? TreatmentCell }.filter { $0 != exceptCell }.forEach {
            $0.isUserInteractionEnabled = isEnabled
        }
    }
    
    func deselectSelectedCells(){
        collectionView.visibleCells.flatMap { $0 as? TreatmentCell }.filter { $0.isPressed }.forEach {
            self.deselectCell($0)
        }
        self.isProgramSelected = false
    }
    
    // MARK: - Start New Treatment Program
    
    /// Start new treatment
    func startTreatment(programType: TreatmentProgram.ProgramType){
        let intensityLevel = Int(intensitySlider.value)
        
        BleManager.manager.startTreatmentProgram(programNum: programType.hashValue, with: intensityLevel)
        let program = TreatmentProgram(type: programType, intensity: intensityLevel)
        ApplicationManager.manager.saveCurrentTreatment(program: program)
        
        changePlusMinusButtonTo(isEnable: true)
    }
    
    /// Start new demo treatment
    func startDemoTreatment(indexPath : IndexPath){
        let selectedProgramType = programsArray[indexPath.item]
        let program = TimerManager.manager.setupTreatmentProgram(with: selectedProgramType, intensity: Int(self.intensitySlider.value))
        if let program = TimerManager.manager.getTreatmentProgram{
            let c = TimerManager.manager.demoModeCounter
            DBManager.manager.saveTreatmentProgram(counter: c.counter, countdown: c.countdown, program: program)
        }
        TimerManager.manager.startTreatmentProgram(totalTimeInSeconds: getTreatmentSelectedTime() ?? 1500)
        performSegue(withIdentifier: Constants.SYMPTOMS_SEGUE, sender: nil)
        
        self.selectCell(at: indexPath)
    }
    
    // MARK: - Stop Treatment Program
    
    func stopTreatmentProgram(){
        BleManager.manager.stopTreatment()
        cleanProgram()
    }
    
    /// Clean program
    /// - parameter deleteProgram: Remove the treatment from db,
    /// You should pass false in case you dont want ro remove data
    /// i.e connection lost but we dont know if program still running
    func cleanProgram(deleteData : Bool = true){
        changePlusMinusButtonTo(isEnable: false)
        timerRemainingLabel.text = "00:00"
        timerStartLabel.text = "00:00"
        progressBar.progress = Float(0)
        deselectSelectedCells()
        intensitySlider.value = 15
        intensityPercentage.text = "15%"
        
        if deleteData{
            ApplicationManager.manager.clearCurrentTreatment()
            DBManager.manager.removeSavedProgram()
        }
    }
    
    // MARK: - Set data
    func setupExistProgram(with program : TreatmentProgram){
        if let index = programsArray.index(of: program.type) {
            let indexPath = IndexPath(item: index, section: 0)
            if collectionView.cellForItem(at: indexPath) != nil{
                selectCell(at: indexPath)
                self.isProgramSelected = true
            }
        }
        
        if let intensity = program.intensity{
            intensityPercentage.text = "\(intensity)%"
            intensitySlider.value = Float(intensity)
        }
        
    }
    
    func setTreatmentData(statusGet : StatusGet){
        let fTime = Float(Float(statusGet.elapsedTime) / Float(statusGet.totalTime))
        self.timerStartLabel.text = statusGet.elapsedTime.formattedTime()
        self.timerRemainingLabel.text = statusGet.totalTime.formattedTime()
        
        
        if !fTime.isNaN{
            self.progressBar.setProgress(fTime, animated: true)
        }
        
        if self.currentAmp == nil && statusGet.amplitude > 0{
            self.currentAmp = statusGet.amplitude
            self.intensityPercentage.text = "\(statusGet.amplitude)%"
            self.intensitySlider.value = Float(statusGet.amplitude)
        }
    }
    
    // MARK: - Set / Get data from UserDefaults
    
    // get the saved hash value of selected program
    // if there is no selected program return 0
    func getFavoriteSavedProgramHashValue() -> Int{
        return UserDefaults.standard.integer(forKey: Constants.MY_TREATMENT_PROGRAM)
    }
    
    // get the Treatment Selected Time value from settings
    func getTreatmentSelectedTime() -> Int?{
        guard let stringTime = UserDefaults.standard.string(forKey: Constants.MY_TREATMENT_PROGRAM_TIME) ,
            let intTime = Int(stringTime)  else{
                return nil
        }
        return intTime * 60
    }
    
    
    /// Save user Intensity in UserDefaults
    private func saveUserIntensity(percentage : Int){
        UserDefaults.standard.set(percentage, forKey: Constants.TREATMENT_INTENSITY)
        if ApplicationManager.manager.isDemoModeEnabled{
            TimerManager.manager.treatmentProgram?.intensity = percentage
        }
    }
    
    private func setIntensity(levelInPercentage: Int){
        if BleManager.manager.isBLEConnected{
            BleManager.manager.setIntensity(levelInPercentage: levelInPercentage)
        }
    }
    
    /// Get user Intensity from UserDefaults
    private func getUserIntensity() -> Int{
        return UserDefaults.standard.integer(forKey: Constants.TREATMENT_INTENSITY)
    }
    
    
}

// MARK: - UICollectionView
extension TreatmentVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return programsArray.count - 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CELL, for: indexPath) as! TreatmentCell
        let item = programsArray[indexPath.item]
        cell.titleLabel.text = item.title
        
        cell.isMyTreatment = item.hashValue == getFavoriteSavedProgramHashValue() ? true : false
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CGFloat((collectionView.frame.size.width / 2) - 2), height: CGFloat(50))
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let cell = collectionView.cellForItem(at: indexPath) as? TreatmentCell else{
            return
        }
        
        // DemoMode enabled
        if ApplicationManager.manager.isDemoModeEnabled{
            
            guard !BleManager.manager.isBLEConnected else {
                
                _ = Utils.alert(title: "error".localized, message: "multi_modes_error".localized, type: .warning)
                
                return
            }
            
        }else{ // Demo mode disabled
            
            // Check if bluetooth connected
            guard BleManager.manager.isBluetoothOn,
                BleManager.manager.isBLEConnected else{
                    
                    _ = Utils.alert(title: "error".localized, message: "disconnected_device_error".localized, type: .warning)
                    
                    return
            }
            
        }
        
        
        // Program is running
        // stop current working program
        if cell.isPressed{

            if ApplicationManager.manager.isDemoModeEnabled{
                TimerManager.manager.stopTimer()
            }else{
                cleanProgram()
                BleManager.manager.stopTreatment()
            }
            
            
        }else{
            
            if ApplicationManager.manager.isDemoModeEnabled{
                startDemoTreatment(indexPath: indexPath)
                return
            }
            
            self.selectCell(at: indexPath)
            let program = programsArray[indexPath.item]
            startTreatment(programType: program)
            // Go to symptom VC only if no program playing
            performSegue(withIdentifier: Constants.SYMPTOMS_SEGUE, sender: nil)
        }
        
        
    }
    
}
extension TreatmentVC : BleManagerDelegate{
    func deviceDidStartTreatment(statusGet: StatusGet, treatmentProgram: TreatmentProgram?) {
        if let program = treatmentProgram {
            self.setupExistProgram(with: program)
        }
    }
    
    //    func deviceDidStoppedTreatment() {
    //        deselectSelectedCells()
    //        cleanProgram()
    //    }
    //
    func deviceConnectionStatusDidChange(to state : BLConnectionState){
        if state == .disconnected{
            self.cleanProgram(deleteData: false)
        }
    }
    
    func bluetoothPowerDidChange(isOn: Bool) {
        if !isOn,!TimerManager.manager.isDemoTreatmentTimerRunning{
            self.cleanProgram(deleteData: false)
        }
    }
    
    func deviceBatteryLevelDidChange(to level : Int){
        
    }
    
    func deviceDidRecivedStatusGet(_ statusGet : StatusGet){
        
        if let program = ApplicationManager.manager.currentTreatment{
            if statusGet.programState == .running, !isProgramSelected{
                self.setupExistProgram(with: program)
            }
        }
        
        self.setTreatmentData(statusGet: statusGet)
        
    }
    
    func deviceAmplitudeDidChange(to level : Int){
        
    }
    
    func deviceDidFinishTreatment() {
        cleanProgram()
    }
}

// MARK: - SymptomDelegate
extension TreatmentVC : SymptomDelegate{
    func userDidFinishAnswer(symptoms: [Symptom]) {
        
        
    }
}

extension TreatmentVC :TimerManagerDelegate{
    func timerDidStop() {
        stopTreatmentProgram()
        deselectSelectedCells()
    }
    
    func timerDidUpdate(timerStart: Int, timerRemaining: Int, progressBar: Float) {
        timerStartLabel.text = timerStart.formattedTime()
        timerRemainingLabel.text = timerRemaining.formattedTime()
        self.progressBar.setProgress(progressBar, animated: true)
    }
    
    
}
