//
//  ViewController.swift
//  UIPageViewController Post
//
//  Created by Maor Shams on 05/05/2017.
//  Copyright © 2017 Seven Even. All rights reserved.
//

import UIKit

class PageViewController: UIViewController {
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var skipTitle: UIButton!
    
    @IBAction func okAction(_ sender: UIButton) {
        
        if isFromSettings{
            self.dismiss(animated: true, completion: nil)
        }else{
            RoutingController.shared.determineRoot()
        }
    }
    
    var isFromSettings : Bool = false
    
    var pageViewVC: PageViewVC? {
        didSet {
            pageViewVC?.pageViewDelegate = self
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageControl.addTarget(self, action: #selector(didChangePageControlValue), for: .valueChanged)
        // setup status bar color
        if let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as? UIView{
            if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
                statusBar.backgroundColor = Constants.Colors.borderLiteGray.value
            }
        }
        
        if !isFromSettings{
            DBManager.manager.saveDidPresentOnBoarding()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let tutorialPageViewController = segue.destination as? PageViewVC {
            self.pageViewVC = tutorialPageViewController
        }
    }
    
    // When the user taps on the pageControl to change its current page.
    @objc func didChangePageControlValue() {
        pageViewVC?.scrollTo(index: pageControl.currentPage)
    }
    
}

extension PageViewController: PageViewVCDelegate {
    
    func didUpdatePageCount(_ vc: PageViewVC, pagesCount : Int) {
        pageControl.numberOfPages = pagesCount
    }
    
    func didUpdatePageIndex(_ vc: PageViewVC, index: Int, pagesCount : Int) {
        if (pagesCount - 1) == index{
            skipTitle.setTitle("Done", for: .normal)
        }else{
            skipTitle.setTitle("Skip intro", for: .normal)
        }
        
        pageControl.currentPage = index
    }
    
    func didFinishPaging() {
        self.dismiss(animated: true, completion: nil)
    }
}
