//
//  Date.swift
//  Theranica
//
//  Created by Maor Shams on 21/02/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit

extension Date{
    var currentDateAndTime : String{
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy HH:mm"
        formatter.locale = .current
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        return formatter.string(from: self)
    }
}
