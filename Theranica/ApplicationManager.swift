//
//  ApplicationManager.swift
//  Theranica
//
//  Created by Maor Shams on 26/11/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit

class ApplicationManager: NSObject {
    
    // MARK: -  Init
    static let manager = ApplicationManager()
    override private init(){}
    
    var isInterfaceOrientationsEnabled = false
    
    // MARK: - Demo mode
    var isDemoModeEnabled : Bool{
        return UserDefaults.standard.bool(forKey: "is_demo_mode_enabled")
    }
    
    func setIsDemoModeEnabled(_ isEnabled : Bool){
        UserDefaults.standard.set(isEnabled, forKey: "is_demo_mode_enabled")
    }
    
    
    // MARK: -  Treatment program for real time treatment (NO DEMOs)
    
    func saveCurrentTreatment(program : TreatmentProgram){
        UserDefaults.standard.set("\(program.type.hashValue)", forKey: Constants.TREATMENT_PROGRAM_HASH)
        UserDefaults.standard.set(program.intensity, forKey: Constants.TREATMENT_INTENSITY)
    }
    
    func clearCurrentTreatment(){
        UserDefaults.standard.set(nil, forKey: Constants.TREATMENT_PROGRAM_HASH)
        UserDefaults.standard.set(nil, forKey: Constants.TREATMENT_INTENSITY)
    }
    
    var currentTreatment : TreatmentProgram?{
        if let programHashString = UserDefaults.standard.string(forKey: Constants.TREATMENT_PROGRAM_HASH),
           let intensity = UserDefaults.standard.value(forKey: Constants.TREATMENT_INTENSITY) as? Int,
           let programHash = Int(programHashString),
           let programType = TreatmentProgram.ProgramType.getType(from: programHash){
            return TreatmentProgram(type: programType, intensity: intensity)
        }
        return nil
    }
    
   
    
    
}
