//
//  Symptom.swift
//  Theranika
//
//  Created by msapps on 21/06/2017.
//  Copyright © 2017 msapps. All rights reserved.
//

import Foundation
import CoreData

extension Symptom{
    
    enum PainLevel : String {
        case none = "None"
        case mid = "Mild"
        case moderate = "Moderate"
        case severe = "Severe"
    
        static var values : [String]{
            return Utils.iterateEnum(Symptom.PainLevel.self).map{
                return $0.rawValue
            }
        }
    
        static func enumCase(from option: String) -> PainLevel?{
            return Utils.enumCase(from: option, of: PainLevel.self)
        }
        
        
        static var key = "painLevel"
    }
    
    enum HeadacheSide : String {
        case none = "None"
        case left = "Left"
        case right = "Right"
        case both = "Both"
        
        static var values : [String]{
            return Utils.iterateEnum(Symptom.HeadacheSide.self).map{
                return $0.rawValue
            }
        }
    
        static func enumCase(from option: String) -> HeadacheSide?{
            return Utils.enumCase(from: option, of: HeadacheSide.self)
        }
        
        static var key = "headacheSide"
    }

}
