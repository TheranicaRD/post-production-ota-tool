//
//  LogInWithMailVC.swift
//  Theranica
//
//  Created by Roei Baruch on 31/08/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit

class LogInWithMailVC: UIViewController {
    @IBOutlet weak var emailTextField: BottomLineTextField!
    @IBOutlet weak var passwordTextField: BottomLineTextField!
    @IBOutlet weak var nextBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpToolbar(textField: emailTextField)
        
        self.nextBtn.isUserInteractionEnabled = false
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LogInWithMailVC.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
        
    }
    
    @IBAction func nextTextField(_ sender: UITextField) {
        if emailTextField.isFirstResponder{
            passwordTextField.becomeFirstResponder()
        }else{
            emailTextField.becomeFirstResponder()
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func textFieldDidChanged(_ sender: Any) {
        let validation = generalValidation()
        if validation {
            self.nextBtn.backgroundColor = UIColor(red:0.03, green:0.41, blue:0.6, alpha:1.0)
            
        }else{
            self.nextBtn.backgroundColor =  UIColor(white:0.7, alpha:1.0)
        }
    }
    func isPasswordMatch(password : String) -> Bool {
        if  password == self.passwordTextField.text {
            return true
        }else{
            return false
        }
    }
    
    func generalValidation() -> Bool {
        guard let passwordField = self.passwordTextField.text,
            let emailField = self.emailTextField.text else{
                return false
        }
        
        let isEmailAddressValid = Utils.isValidEmail(emailField)
        
        let isPasswordValidate = passwordField.count >= 6
        
        
        
        
        
        if (isEmailAddressValid && isPasswordValidate){
            self.nextBtn.isUserInteractionEnabled = true
            return true
        }else{
            self.nextBtn.isUserInteractionEnabled = false
            return false
        }
        
    }
    
    @IBAction func nextBtn(_ sender: Any) {
        
        guard let passwordField = self.passwordTextField.text,
            let emailField = self.emailTextField.text else{
                return
        }
        let loadingAlert = Utils.alert(title: "Verifying...", message : "Please wait while connecting" , type: .wait, autoHide : false)
        
        AuthManager.manager.loginWithMail(password: passwordField, email: emailField) { (error) in
            
            loadingAlert.close()
            
            if let error = error {
                _ = Utils.alert(title: "Error", message: (error.localizedDescription), type: .error)
                return
            }
            RoutingController.shared.determineRoot()
        }
    }
    
    @IBAction func resetPasswordAction(_ sender: UIButton) {
        if let nextVC = storyboard?.instantiateViewController(withIdentifier: "resetPasswordVC"){
            self.navigationController?.pushViewController(nextVC, animated: true)
        }
    }
}


extension LogInWithMailVC : UITextFieldDelegate{
    
    func setUpToolbar(textField: UITextField) {
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        toolBar.barStyle = UIBarStyle.default
        toolBar.items = [
            UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.plain, target: self, action:#selector(nextTextField(_:))),
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
        ]
        toolBar.sizeToFit()
        
        textField.inputAccessoryView = toolBar
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let passwordField = self.passwordTextField.text,
            let emailField = self.emailTextField.text else{
                return
        }
        
        guard let textField = textField as? BottomLineTextField else{
            return
        }
        
        if textField == self.emailTextField {
            let isEmailAddressValid = Utils.isValidEmail(emailField)
            textField.hasError = !isEmailAddressValid
            return
        }else if textField == self.passwordTextField {
            let isPasswordValidate = passwordField.count >= 6
            textField.hasError = !isPasswordValidate
            return
        }
    }
    
}
