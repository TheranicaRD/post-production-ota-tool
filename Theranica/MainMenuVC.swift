//
//  MainMenuVC.swift
//  Theranika
//
//  Created by msapps on 15/06/2017.
//  Copyright © 2017 msapps. All rights reserved.
//

import UIKit

class MainMenuVC: UIViewController {
    
    enum Menu : String {
        case treatment = "TREATMENT"
        case migraineDiary = "MIGRAINE DIARY"
        case migraineEducationToolbox = "MIGRAINE EDUCATION TOOLBOX"
        case painManagementToolbox = "PAIN MANAGEMENT TOOLBOX"
        case orderPatches = "ORDER PATCHES"
        case settings = "SETTINGS"
        case help = "HELP"
        
        var segue : String{
            switch self {
            case .treatment : return "treatmentSegue"
            case .migraineDiary : return "migraineDiarySegue"
            case .migraineEducationToolbox : return "migraineEducationSegue"
            case .painManagementToolbox : return "painManagmentSegue"
            case .orderPatches : return "orderPatchesSegue"
            case .settings : return "settingsSegue"
            case .help : return ""
            }
        }
        
        var image : UIImage{
            switch self {
            case .treatment : return #imageLiteral(resourceName: "image_treatments")
            case .migraineDiary : return #imageLiteral(resourceName: "image_diary")
            case .migraineEducationToolbox : return #imageLiteral(resourceName: "image_toolbox")
            case .painManagementToolbox : return #imageLiteral(resourceName: "image_toolbox")
            case .orderPatches : return #imageLiteral(resourceName: "image_cart")
            case .settings : return #imageLiteral(resourceName: "image_settings")
            case .help : return #imageLiteral(resourceName: "image_helpicon")
            }
        }
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    let menuButtons : [MainMenuVC.Menu] = [
        .treatment,
        .migraineDiary,
        .migraineEducationToolbox,
        .painManagementToolbox,
        .orderPatches,
        .settings,
        .help
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.alpha = 0
        
        if DBManager.manager.isOldVersionOfCoreData(){
            _ = Utils.alert(title: "Old DB Version Error",
                        message: "Please Delete and install Theranica",
                        type: .error, showCloseButton: false, autoHide: false)
            return
        }
        
        LocationManager.manager.requestAuth()
        
        // if there are saved treatments, send to DB
        NetworkManager.manager.sendSavedDiaryToDB()
        
        
        
//        #if DEBUG
//            let item1 = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(gotoDebug))
//            self.navigationItem.setRightBarButtonItems([item1], animated: true)
//        #endif
    }

    @objc func gotoDebug(){
        let storyBoardName = RoutingController.Storyboard.empty.rawValue
        let storyBoard = UIStoryboard(name: storyBoardName, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "debugVC") as! DebugVC
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.transition(with: tableView, duration: 0.8, options: .curveLinear, animations: {
            self.tableView.alpha = 1
        }, completion: nil)
        
    }
    

}

// MARK: - TableView
extension MainMenuVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return menuButtons.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CELL) as! MainMenuCell
        cell.titleLabel.text = menuButtons[indexPath.section].rawValue
        cell.titleIcon.image = menuButtons[indexPath.section].image
        cell.circled(with : 3)
        cell.accessoryView?.backgroundColor = .white
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let tableViewHeight = tableView.bounds.height
        let menuItems = CGFloat(menuButtons.count)
        
        return (tableViewHeight / menuItems) * 0.8
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let cell = tableView.cellForRow(at: indexPath) else{
            return
        }
        
        let segueID : String = menuButtons[indexPath.section].segue
        
        if !segueID.isEmpty{
            performSegue(withIdentifier: segueID, sender: cell)
        }else {
            // In dispatch because the double click bug.
            DispatchQueue.main.async {
                let onBoardingStoryBoard = UIStoryboard(name: "OnBoarding", bundle: nil)
                if let onBoardingVC = onBoardingStoryBoard.instantiateViewController(withIdentifier: "PageViewController") as? PageViewController{
                    onBoardingVC.isFromSettings = true
                self.present(onBoardingVC, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) else{
            return
        }
        
        cell.contentView.backgroundColor = Constants.Colors.selectedMenu.value
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) else{
            return
        }
        
        cell.contentView.backgroundColor = Constants.Colors.menuCell.value
    }
}

