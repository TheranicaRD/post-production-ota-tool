//
//  MainMenuCell.swift
//  Theranika
//
//  Created by msapps on 15/06/2017.
//  Copyright © 2017 msapps. All rights reserved.
//

import UIKit

class MainMenuCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleIcon: UIImageView!
    
    override var frame: CGRect{
        get {
            return super.frame
        }
        set (newFrame) {
            let inset: CGFloat = 4
            var frame = newFrame
            frame.origin.x += inset
            frame.size.width -= 2 * inset
            super.frame = frame
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor(red: 131/255, green: 179/255, blue: 204/255, alpha: 1)
    }

}
