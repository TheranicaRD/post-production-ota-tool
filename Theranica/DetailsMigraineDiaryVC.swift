//
//  DetailsMigraineDiaryVC.swift
//  Theranika
//
//  Created by msapps on 21/06/2017.
//  Copyright © 2017 msapps. All rights reserved.
//

import UIKit

class DetailsMigraineDiaryVC: UIViewController {
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var middleLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    var treatment : Treatment?
    var symptoms = [Symptom]()
    
    var updatedSymptoms = [Symptom]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var address  = "Unknown"
        var weather = "Unknown"
        
        guard let treatment = treatment,
            let date = treatment.date,
            let symptoms = treatment.symptoms as? Set<Symptom> else{
                return
        }
        
        if let _address = treatment.location?.address{
            address = _address
        }
        
        if let _weather = treatment.location?.weather{
            weather = _weather
        }
        
        topLabel.text = Utils.detailsMigraineDiaryTopFormat(date: date as Date)
        middleLabel.text = "\(Utils.detailsMigraineDiaryMiddleFormat(date: date as Date)), \(address)"
        bottomLabel.text = weather
        
        //old sort
//        self.symptoms = Array(symptoms).sorted { ($0.name ?? "").localizedCaseInsensitiveCompare($1.name ?? "") == .orderedAscending }
        
        //new sort
        let knownOrder = ["headache level", "headache side", "nausea", "aura","photophobia", "phonophobia"]
        self.symptoms = Array(symptoms).sorted {
            knownOrder.index(of: $0.name?.lowercased() ?? "") ?? 0 < knownOrder.index(of: $1.name?.lowercased() ?? "") ?? 0
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        updateUserSymptoms()
    }
    
    func updateUserSymptoms(){
        
        guard let treatmentID = treatment?.id,
            !updatedSymptoms.isEmpty else{
                return
        }
        if Reachability.isConnectedToNetwork(){
            NetworkManager.manager.updateUserDiary(treatmentID: treatmentID, symptoms: symptoms)
        }
    }
}
extension DetailsMigraineDiaryVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return symptoms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CELL) as! DetailsMigraineDiaryCell
        let index = indexPath.row
        let currentSymptom = symptoms[index]
        cell.configCell(with: currentSymptom, at : index)
        cell.delegate = self
        return cell
    }
}
extension DetailsMigraineDiaryVC : DetailsMigraineDiaryCellDelegate{
    func symptomDidUpdate(at index: Int, symptom: Symptom) {
        if Reachability.isConnectedToNetwork(){
            symptoms[index] = symptom
            tableView.reloadData()
            updatedSymptoms.append(symptom)
        }else{
            _ = Utils.alert(title: "Error", message: "No internet Connection, Please try again..", type: .error, showCloseButton: true, autoHide: false)
        }
    }
}
