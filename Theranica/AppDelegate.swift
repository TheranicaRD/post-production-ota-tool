//
//  AppDelegate.swift
//  Theranica
//
//  Created by Maor Shams on 26/06/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit
import FacebookCore
import Firebase
import Google

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        /// Auth -
        
        // Firebase
        FirebaseApp.configure()
        
        // Facebook
        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        // Google
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        
        // determine the storyboard
        RoutingController.shared.window = window
        RoutingController.shared.determineRoot()
        
        if BleManager.manager.isBluetoothOn{
            BleManager.manager.startScan()
        }
        
        if ApplicationManager.manager.isDemoModeEnabled{
            TimerManager.manager.fetchTreatmentProgram()
        }
        return true
    }
    
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        let oriantation = ApplicationManager.manager.isInterfaceOrientationsEnabled ? UIInterfaceOrientationMask.all.rawValue : UIInterfaceOrientationMask.portrait.rawValue
        UIApplication.shared.isStatusBarHidden = false
        return UIInterfaceOrientationMask(rawValue: oriantation)
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
       
    
        if let program = TimerManager.manager.getTreatmentProgram{
            let c = TimerManager.manager.demoModeCounter
            DBManager.manager.saveTreatmentProgram(counter: c.counter, countdown: c.countdown, program: program)
        }
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        TimerManager.manager.fetchTreatmentProgram()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
       
       
        if let program = TimerManager.manager.getTreatmentProgram{
            let c = TimerManager.manager.demoModeCounter
            DBManager.manager.saveTreatmentProgram(counter: c.counter, countdown: c.countdown, program: program)
        }
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let facebookDidHandle = SDKApplicationDelegate.shared.application(app, open:url, options:options)
        let googleDidHandle = GIDSignIn.sharedInstance().handle(url,
                                                                sourceApplication:options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                                annotation: [:])
        
        return facebookDidHandle || googleDidHandle
    }
    
    
    
}



