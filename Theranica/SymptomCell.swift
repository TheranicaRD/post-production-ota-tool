//
//  SymptomCell.swift
//  Theranika
//
//  Created by msapps on 21/06/2017.
//  Copyright © 2017 msapps. All rights reserved.
//

import UIKit
protocol SymptomCellDelegate {
    func symptomDidSelect(symptom : Symptom, selectedButtonTag : Int)
    func symptomDidDeselect(symptom : Symptom, selectedButtonTag : Int)
}

class SymptomCell: UICollectionViewCell {
    
    @IBOutlet weak var symptomName: UILabel!
    @IBOutlet weak var optionButton1: UIButton!
    @IBOutlet weak var optionButton2: UIButton!
    @IBOutlet weak var optionButton3: UIButton!
    @IBOutlet weak var optionButton4: UIButton!
    
    var optionButtons : [UIButton] = []
    var delegate : SymptomCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addButtonsToArray()
        
        // Set corner radius to buttons
        for button in optionButtons{
            button.circled(with: 4)
        }
    }
    
    // MARK: - Config Cell
    func configCell(with symptomName : String , options :  [String], selectedButtonTag : Int? = nil, isHeadacheLevel : Bool){
        
        // If there is already selected button, select it
        if let selectedButtonTag = selectedButtonTag{
            for button in optionButtons{
                if button.tag == selectedButtonTag{
                    button.isSelected = true
                }
            }
        }
        
        // set the symptom name
        self.symptomName.text = symptomName
        
        // set the title for options buttons
        for i in 0..<optionButtons.count{
            optionButtons[i].setTitle(options[i], for: .normal)
        }
        
        if isHeadacheLevel{
            optionButtons.first?.isHidden = true
        }
    }
    
    // Add the buttons to array
    func addButtonsToArray(){
        optionButtons = []
        optionButtons.append(optionButton1)
        optionButtons.append(optionButton2)
        optionButtons.append(optionButton3)
        optionButtons.append(optionButton4)
    }
    
    // MARK: - On click button
    @IBAction func clickAction(_ sender: UIButton) {
        
        // When clicking on button, deselect others
        deselectSelectedButtons(notifyCaller : true)
        
        // Select current
        sender.isSelected = !sender.isSelected
        
        // Get selected Button Title
        guard let selectedButtonTitle = sender.titleLabel?.text,
            let symptomName = symptomName.text,
            let symptom = DBManager.manager.generateSymptom(symptomName: symptomName, buttonTitle: selectedButtonTitle) else{
                return
        }
        
        self.delegate?.symptomDidSelect(symptom: symptom, selectedButtonTag: sender.tag)
    }
    
    // Check for selected butttons, if selected deselect
    func deselectSelectedButtons(notifyCaller : Bool = false){
        
        for button in optionButtons{
            
            if button.isSelected{
                
                button.isSelected = false
                if notifyCaller == true,
                    let selectedButtonTitle = button.titleLabel?.text,
                    let symptomName = symptomName.text,
                    let symtom = DBManager.manager.generateSymptom(symptomName: symptomName, buttonTitle: selectedButtonTitle){
                    self.delegate?.symptomDidDeselect(symptom: symtom, selectedButtonTag: button.tag)
                    
                }
            }
        }
    }
    
    
    // Performs any clean up necessary to prepare the view for use again.
    override func prepareForReuse() {
        deselectSelectedButtons()
        optionButton1.isHidden = false
    }
}
