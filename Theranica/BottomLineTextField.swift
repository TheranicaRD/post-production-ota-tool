//
//  BottomLineTextField.swift
//  Theranica
//
//  Created by Maor Shams on 28/08/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit

class BottomLineTextField: UITextField {
    
    var bottomBorder = UIView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setBottomBorder()
    }
    
    func setBottomBorder() {
        // Setup Bottom-Border
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        bottomBorder = UIView.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        hasError = false
        bottomBorder.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(bottomBorder)
        
        bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        bottomBorder.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        bottomBorder.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true // Set Border-Strength
        
        
    }
    
    
    var hasError: Bool = false {
        didSet {
            
            if (hasError) {
                
                bottomBorder.backgroundColor = UIColor.red
                
            } else {
                
                bottomBorder.backgroundColor = UIColor(white: 0.93, alpha: 1)
                
            }
            
        }
    }
}
