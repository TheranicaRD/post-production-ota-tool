//
//  Debouncer.swift
//  Theranica
//
//  Created by Maor Shams on 19/02/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import Foundation

typealias Debounce<T> = (_ : T) -> Void

func debounce<T>(interval: TimeInterval, queue: DispatchQueue, action: @escaping Debounce<T>) -> Debounce<T> {
    var lastFireTime = DispatchTime.now()
    return { param in
        lastFireTime = DispatchTime.now()
        let dispatchTime: DispatchTime = DispatchTime.now() + interval
        
        queue.asyncAfter(deadline: dispatchTime) {
            let when: DispatchTime = lastFireTime + interval
            let now = DispatchTime.now()
            
            if now.rawValue >= when.rawValue {
                action(param)
            }
        }
    }
}
