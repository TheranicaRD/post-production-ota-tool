//
//  MigraineEducationCell.swift
//  Theranica
//
//  Created by msapps on 29/06/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit

class MigraineEducationCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override var frame: CGRect{
        get {
            return super.frame
        }
        set (newFrame) {
            let inset: CGFloat = 4
            var frame = newFrame
            frame.origin.x += inset
            frame.size.width -= 2 * inset
            super.frame = frame
        }
    }


}
