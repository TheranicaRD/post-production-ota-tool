//
//  TimerManager.swift
//  Theranica
//
//  Created by Maor on 12/07/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit

class TimerManager : NSObject{
    
    static let manager = TimerManager()
    override private init() {}
    
    var delegate : TimerManagerDelegate?
    private var demoTreatment = Timer()
    private var statusGetTimer = Timer()
    
    private var counter = 0
    private var countdown = 0
    private var countUp = 0
    
    var treatmentProgram : TreatmentProgram?
    
    var fractionalProgress : Float{
        return Float(countUp) / Float(counter)
    }
    
    var isDemoTreatmentTimerRunning : Bool{
        return demoTreatment.isValid
    }
    var isStatusGetTimerRunning : Bool{
        return statusGetTimer.isValid
    }
    
    var getTreatmentProgram : TreatmentProgram?{
        return treatmentProgram
    }
    
    var demoModeCounter : (counter : Int, countdown : Int, countUp : Int){
        return (counter,countdown,countUp)
    }
    
    
    func setupTreatmentProgram(with programType : TreatmentProgram.ProgramType, intensity : Int) -> TreatmentProgram{
        let treatmentProgram = TreatmentProgram(type: programType, intensity: intensity)
        self.treatmentProgram = treatmentProgram
        return treatmentProgram
    }
    
    func startTreatmentProgram(totalTimeInSeconds totalTime : Int){
        
        guard let treatmentProgram = treatmentProgram else{
            return
        }
        
        treatmentProgram.totalTime = totalTime
        
        self.counter   = totalTime
        self.countdown = totalTime
        self.countUp = 0
        
        if demoTreatment.isValid{
            stopTimer()
        }
        
        demoTreatment = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
        demoTreatment.fire()
    }
    
    func setupSavedTimer(programTime : Int, countDown : Int, countUp : Int){
        self.counter = programTime
        self.countdown = countDown
        self.countUp = countUp
        demoTreatment = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
        demoTreatment.fire()

    }
    
    /// function that called each certain time
  @objc func updateCounter(){
        // countdown still possible
        if countdown > 0 {
            countdown -= 1
        }else{
            // countdown not possible
            stopTimer()
            return
        }
        
        if countUp >= 0{
            countUp += 1
        }
        self.delegate?.timerDidUpdate(timerStart: countUp,
                                      timerRemaining: countdown,
                                      progressBar: fractionalProgress)
    }
    
    /// Stop current working timer
    /// - parameters:
    ///   - deselectCell: Also deselect current pressed cell?
    func stopTimer(){
        demoTreatment.invalidate()
        self.delegate?.timerDidStop()
        self.treatmentProgram = nil
        DBManager.manager.removeSavedProgram()
    }
    
    
    func fetchTreatmentProgram(){
        guard  let programName = UserDefaults.standard.value(forKey: Constants.TREATMENT_PROGRAM_HASH) as? Int,
            let type = TreatmentProgram.ProgramType.getType(from: programName),
            let totalProgramTime = UserDefaults.standard.value(forKey: Constants.TREATMENT_TOTAL_TIME) as? Int,
            let programSaveTime = UserDefaults.standard.value(forKey: Constants.TREATMENT_SAVE_TIME) as? Date,
            let timePassed = UserDefaults.standard.value(forKey:  Constants.TREATMENT_PASSED_TIME) as? Int,
            let intensity = UserDefaults.standard.value(forKey:  Constants.TREATMENT_INTENSITY) as? Int else{
                return
        }
        
      
        
        self.treatmentProgram = TreatmentProgram(type: type, totalTime: totalProgramTime, intensity: intensity)
        
        let timePassedFromSave =  Date().timeIntervalSince(programSaveTime)
        let totalPassedTime = Int(timePassedFromSave) + Int(timePassed)
        let totalLeftTime = Int(totalProgramTime - totalPassedTime)
        
        setupSavedTimer(programTime: totalProgramTime,
                        countDown: totalLeftTime,
                        countUp: totalPassedTime)
    }
    
    // Status get timer
    
    func startStatusGetTimer(){
        if statusGetTimer.isValid{
           statusGetTimer.invalidate()
        }
        
        statusGetTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(updateStatusGet), userInfo: nil, repeats: true)
        statusGetTimer.fire()
    }
    
    func stopStatusGetTimer(){
        statusGetTimer.invalidate()
    }
    
    @objc func updateStatusGet(){
        BleManager.manager.getStatus()
    }
}

protocol TimerManagerDelegate {
    
    func timerDidUpdate(timerStart : Int, timerRemaining : Int, progressBar : Float)
    
    func timerDidStop()
    
}
