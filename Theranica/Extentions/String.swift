//
//  String.swift
//  Theranica
//
//  Created by Maor Shams on 21/02/2018.
//  Copyright © 2018 MSApps. All rights reserved.
//

import UIKit
extension String{
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    var propercased : String{
        let first = String(prefix(1)).capitalized
        let other = String(dropFirst()).lowercased()
        return first + other
    }
    var camelcased : String {
        let strings = self.lowercased().components(separatedBy: " ")
        var firstComponent = strings[0]
        
        for i in 1 ..< strings.count{
            var string = strings[i]
            guard let char = string.first else { continue }
            string.remove(at: string.startIndex)
            var firstLowerCase = String(describing: char).uppercased()
            firstLowerCase.append(string)
            firstComponent.append(firstLowerCase)
        }
        
        return firstComponent
    }
    
    func toDateUTC () -> Date?{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return formatter.date(from: self)
    }
    
}
