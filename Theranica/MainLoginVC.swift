//
//  MainLoginVC.swift
//  Theranica
//
//  Created by Roei Baruch on 20/08/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit
import Google
import SCLAlertView

class MainLoginVC: UIViewController,GIDSignInUIDelegate {
    
    var loaderHandler : (isLoginClicked : Bool,isRecivedData : Bool) = (false,false)
    var loadingAlert  :  SCLAlertViewResponder?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AuthManager.manager.delegate = self
        
        Utils.setStatusBarColor()
        
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if loaderHandler.isLoginClicked && !loaderHandler.isRecivedData{
            loaderHandler = (false,false)
            showLoader()
        }
    }
    
    
    @IBAction func fbButtonAction(_ sender: Any) {
        if !Reachability.isConnectedToNetwork(){
            noInternetConnectionAlert()
            return
        }
        
        loaderHandler = (isLoginClicked : true,isRecivedData : false)
        AuthManager.manager.requestFacebookAuth(on: self)
    }
    
    @IBAction func gmailButtonAction(_ sender: Any) {
        if !Reachability.isConnectedToNetwork(){
            noInternetConnectionAlert()
            return
        }
        loaderHandler = (isLoginClicked : true,isRecivedData : false)
        AuthManager.manager.requestGmailAuth()
        if GIDSignIn.sharedInstance().hasAuthInKeychain(){
            showLoader()
        }
    }
    
    func noInternetConnectionAlert(){
        _ = Utils.alert(title: Constants.ERROR, message: "No Internet connection", type: .error)
    }
    
    private func showLoader(){
        loadingAlert = Utils.alert(title: "Verifying...", message : "Please wait while connecting" , type: .wait , autoHide : false)
    }
    
    fileprivate func handleAuthError(with error : Error){
        _ = Utils.alert(title: Constants.ERROR, message : error.localizedDescription , type: .error)
    }
}
extension MainLoginVC : AuthManagerDelegate{
    
    func didFinishFacebookAuth(error: Error?) {
        self.loaderHandler.isRecivedData = true
        self.loadingAlert?.close()
        
        if let error = error{
            handleAuthError(with: error)
        }
    }
    
    func didFinishGoogleAuth(error: Error?) {
        self.loaderHandler.isRecivedData = true
        self.loadingAlert?.close()
        
        if let error = error{
            handleAuthError(with: error)
        }
    }
}
