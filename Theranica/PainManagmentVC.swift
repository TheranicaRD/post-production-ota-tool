//
//  PainManagmentVC.swift
//  Theranica
//
//  Created by Maor on 03/07/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit

class PainManagmentVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    typealias PainManagmentMenuItem = (text : String, image : UIImage, url : String)
    
    let menuItems : [PainManagmentMenuItem] = [
        ("Pain Relief, Guided Imagery",#imageLiteral(resourceName: "image_video"),"BgOKKjgqqMc"),
        ("Relaxed Breathing",#imageLiteral(resourceName: "image_video"),"87buSvmaP4s"),
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideBackButton()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nextVC = segue.destination as? WebviewVC, let indexPath = sender as? IndexPath{
            let item = menuItems[indexPath.section]
            nextVC.videoCode = item.url
            nextVC.headTitle = item.text
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
}
extension PainManagmentVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CELL, for: indexPath) as! MigraineEducationCell
        cell.iconImageView.image = menuItems[indexPath.section].image
        cell.titleLabel.text = menuItems[indexPath.section].text
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "PainManagmentToWebView", sender: indexPath)
    }
}
