//
//  TDeviceManagerDelegate.swift
//  TheranicaBLE
//
//  Created by Maor Shams on 10/09/2017.
//  Copyright © 2017 Maor Shams. All rights reserved.
//

import UIKit
import CoreBluetooth

@objc public protocol TDeviceManagerDelegate{
    
    @objc optional func peripheralDidDiscover(peripheral: CBPeripheral)
    @objc optional func peripheralDidDiscover(characteristic: CBCharacteristic)
    @objc optional func peripheralDidUpdateValue(for characteristic: CBCharacteristic)
    
    func bluetoothPowerDidChange(isOn: Bool)

    func tDeviceDidRecivedStatusGet(_ statusGet : StatusGet)
    func tDeviceDidRecivedVersion(version : String )
    func tDeviceConnectionStatusDidChange(to state : BLConnectionState)
    func tDeviceBatteryLevelDidChange(to level : Int,statusGet : StatusGet)
    func tDeviceAmplitudeDidChange(to level : Int,statusGet : StatusGet)
    func tDeviceDidStartTreatment(_ statusGet : StatusGet)
    func tDeviceDidStoppedTreatment(statusGet : StatusGet?)
    func tDeviceDidFinishedTreatment(statusGet : StatusGet)
}
