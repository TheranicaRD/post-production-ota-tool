//
//  WebviewVC.swift
//
//
//  Created by Maor on 14/08/2017.
//
//

import UIKit
import SCLAlertView
import youtube_ios_player_helper

class WebviewVC: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var youtubePlayer: YTPlayerView!
    
    var url : String?
    var videoCode: String?
    var loadingAlert : SCLAlertViewResponder?
    var headTitle : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard Reachability.isConnectedToNetwork() else {
            _ = Utils.alert(title: Constants.ERROR,
                            message: "No Internet connection, Please try again ..",
                            type: .error, showCloseButton: true, autoHide: false, completion : {
                                
                                self.navigationController?.popViewController(animated: true)
            })
            return
        }
     
        self.title = self.headTitle
        
        loadingAlert = Utils.alert(title: "Loading", message: "Please wait", type: .wait , autoHide : false)
        
        
        if let url = self.url{
            self.loadWeb(with: url)
            self.youtubePlayer.isHidden = true
        }
        
        if let code = self.videoCode{
            youtubePlayer.load(withVideoId: code)
            youtubePlayer.delegate = self
            self.webView.isHidden = true
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ApplicationManager.manager.isInterfaceOrientationsEnabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        ApplicationManager.manager.isInterfaceOrientationsEnabled = false
    }
    
    func loadVideo(with code : String){
        guard let url = URL(string: "https://www.youtube.com/embed/\(code)") else{
            return
        }
        
        let request = URLRequest(url: url)
        webView.loadRequest(request)
    }
    
    
    func loadWeb(with url : String){
        guard let url = URL(string: url) else{
            return
        }
        
        let request = URLRequest(url: url)
        webView.loadRequest(request)
    }
    
    
}
extension WebviewVC : UIWebViewDelegate{
    func webViewDidFinishLoad(_ webView: UIWebView) {
        loadingAlert?.close()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        loadingAlert?.close()
    }
}
extension WebviewVC : YTPlayerViewDelegate{
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        loadingAlert?.close()
    }
    func playerView(_ playerView: YTPlayerView, receivedError error: YTPlayerError) {
        loadingAlert?.close()
    }
}
