//
//  ProfileVC.swift
//  Theranica
//
//  Created by Maor Shams on 07/09/2017.
//  Copyright © 2017 MSApps. All rights reserved.
//

import UIKit
import SCLAlertView

class ProfileVC: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var caregiverNameTextField: UITextField!
    @IBOutlet weak var caregiverRoleTextField: UITextField!
    @IBOutlet weak var caregiverEmailTextField: UITextField!
    @IBOutlet weak var caregiverShareSwitch: UISwitch!
    @IBOutlet weak var saveButton: UIButton!
    
    private var passwordToChange : String?
    private var emailToChange    : String?
    private var loadingAnimation : SCLAlertViewResponder?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let providerID = AuthManager.manager.currentUserProviderID,
            providerID == "password" {
            emailTextField.text = AuthManager.manager.currentUser?.email
        }else{
            saveButton.isHidden = true
            emailTextField.isEnabled = false
            passwordTextField.isEnabled = false
            emailTextField.text = AuthManager.manager.currentUser?.displayName
        }
        
        if let caregiverName = UserDefaults.standard.string(forKey: Constants.CAREGIVER_NAME){
            caregiverNameTextField.text = caregiverName
        }
        
        if let caregiverRole = UserDefaults.standard.string(forKey: Constants.CAREGIVER_ROLE){
            caregiverRoleTextField.text = caregiverRole
        }
        
        if let caregiverEmail = UserDefaults.standard.string(forKey: Constants.CAREGIVER_EMAIL){
            caregiverEmailTextField.text = caregiverEmail
        }
        
        if let isCaregiverShareOn = UserDefaults.standard.value(forKey: Constants.CAREGIVER_SHARE) as? Bool{
            self.caregiverShareSwitch.isOn = isCaregiverShareOn
        }
        
    }
    
    @IBAction func textFieldDidEndOnExit(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
    
    @IBAction func textFieldEditingChanged(_ sender: UITextField) {
        guard let text = sender.text else {
            return
        }
        
        if sender === emailTextField{
            emailToChange = text
        }else if sender === passwordTextField{
            passwordToChange = text
        }
    }
    
    @IBAction func saveButtonAction(_ sender: UIButton) {
        
        if emailToChange == nil && passwordToChange == nil{
            return
        }
        
        self.view.endEditing(true)
        
        if let email = emailToChange{
            
            guard !email.isEmpty else{
                showError(message: "Password/Email cannot be empty..")
                return
            }
            
            guard Utils.isValidEmail(email) else{
                showError(message: "Email is not valid.")
                return
            }
            
            self.emailToChange = email
        }
        
        if let password = passwordToChange{
            guard !password.isEmpty else{
                showError(message: "Password/Email cannot be empty..")
                return
            }
            
            guard Utils.isPasswordValid(password) else{
                showError(message: "Password is not valid.")
                return
            }
            
            self.passwordToChange = password
        }
        
        // Show validate old password
        showPasswordValidationAlert { oldPassword in
            
            self.loadingAnimation = Utils.alert(title: "Loading", message: "Please wait", type: .wait, autoHide: false)
            
            //reauthenticate
            AuthManager.manager.reauthenticate(currentPassword: oldPassword, completion: { (error) in
                
                // reauthenticate error
                if let error = error{
                    self.loadingAnimation?.close()
                    self.showError(message: error.localizedDescription)
                    return
                }
                
                // if the user change email & passwors
                if let email = self.emailToChange, let password = self.passwordToChange{
                    
                    self.changeEmailAndPasword(email: email, password: password)

                 // if the user is change email
                }else if let email = self.emailToChange{
                    
                    self.changeUserEmail(email: email){ error in
                        self.loadingAnimation?.close()
                        if let error = error{
                            self.showError(message: error.localizedDescription)
                            return
                        }
                        _ = Utils.alert(title: "Success", message: "Email has been changed successfully", type: .success)
                    }
                    
                 // if the password is changed
                }else if let password = self.passwordToChange{
                    self.changeUserPassword(password: password){ error in
                        self.loadingAnimation?.close()
                        if let error = error{
                            self.showError(message: error.localizedDescription)
                            return
                        }
                        
                        _ = Utils.alert(title: "Success", message: "Password has been changed successfully", type: .success)
                    }
                }
                
            })
        }
    }
    
    private func changeUserEmail(email : String, completion : @escaping (Error?)->Void){
        AuthManager.manager.changeEmail(to: email) { completion($0) }
    }
    
    private func changeUserPassword(password : String, completion : @escaping (Error?)->Void){
        AuthManager.manager.changePassword(to: password) { completion($0) }
    }
    
    @IBAction func didEndOnExit(_ sender: UITextField) {
        sender.resignFirstResponder()
        
        guard let text = sender.text, !text.isEmpty else{
            return
        }
       
        if sender === caregiverNameTextField{
            UserDefaults.standard.set(text, forKey: Constants.CAREGIVER_NAME)
        }else if sender === caregiverRoleTextField{
            UserDefaults.standard.set(text, forKey: Constants.CAREGIVER_ROLE)
        }else if sender === caregiverEmailTextField{
            UserDefaults.standard.set(text, forKey: Constants.CAREGIVER_EMAIL)
        }
    }
    
    @IBAction func caregiverShareAction(_ sender: UISwitch) {
        UserDefaults.standard.set(sender.isOn, forKey: Constants.CAREGIVER_SHARE)
    }
    
    
    private func changeEmailAndPasword(email : String, password : String){
        self.changeUserEmail(email: email, completion: { (error) in
            
            if let error = error{
                self.loadingAnimation?.close()
                self.showError(message: error.localizedDescription)
                return
            }
            
            self.changeUserPassword(password: password, completion: { (error) in
                
                self.loadingAnimation?.close()
                
                if let error = error{
                    self.showError(message: error.localizedDescription)
                    return
                }
                
                _ = Utils.alert(title: "Success", message: "Email and Password has been changed successfully", type: .success)
                
            })
        })
    }
    
    private func showError(message : String){
        _ = Utils.alert(title: Constants.ERROR, message: message, type: .error, autoHideTime : 3)
    }
    
    private func showPasswordValidationAlert(completion : @escaping (String)->Void){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        
        let alert = SCLAlertView(appearance: appearance)
        let textField = alert.addTextField("Your old password")
        textField.isSecureTextEntry = true
        
        alert.addButton("Ok") {
            
            guard let password = textField.text, !password.isEmpty else{
                _ = Utils.alert(title: "Error", message: "Invalid device password", type: .error)
                return
            }
            
            completion(password)
        }
        
        alert.addButton("Cancel", backgroundColor: .lightGray, textColor: .black, showDurationStatus: false) {}
        
        alert.showEdit("Password validation", subTitle: "Please enter your old password",colorStyle: 0x8AC341)
    }
    
    
}

